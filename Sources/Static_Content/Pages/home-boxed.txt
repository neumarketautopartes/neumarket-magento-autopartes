{{widget type="Magento\Cms\Block\Widget\Block" template="widget/static_block/default.phtml" block_id="porter_content_slider"}}
<hr class="indent-18 white-space">
<div class="row">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_banner_1.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_banner_2.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-4 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_banner_3.jpg'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Handpicked products</h3>
</header>
	{{widget type="Meigee\ProductWidget\Block\Product\ProductsList" widget_type="featuredcategory" featured_category="2" collection_sort_by="name" collection_sort_order="asc" show_pager="0" products_count="5" template="Meigee_ProductWidget::product/widget/content/slider.phtml" visible_products="4" visible_products_tablet="3" visible_products_mobile="1" pagination="false" widget_id="9207" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^]^]"}}
<hr class="indent-40 white-space">
<div class="row">
	<div class="col-sm-6 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_banner_4.jpg'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-6 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_banner_5.jpg'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Blazers</h3>
</header>
<div class="product-grid-wrapper">
	{{widget type="Meigee\ProductWidget\Block\Product\ProductsList" widget_type="newproducts" collection_sort_by="name" collection_sort_order="asc" show_pager="0" products_count="4" template="Meigee_ProductWidget::product/widget/content/list.phtml" widget_id="3355" conditions_encoded="^[`1`:^[`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`aggregator`:`all`,`value`:`1`,`new_child`:``^]^]"}}
</div>
<hr class="indent-40 white-space">
<header class="widget-title">
	<h3>Our brands</h3>
</header>
<hr class="indent-18 white-space">
<div class="row">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_1.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_2.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_3.png'}}" alt="">
		</a>
	</div>
	<hr class="indent-18 white-space visible-xs">
	<div class="col-sm-3 text-center">
		<a href="#" title="Banner" class="banner">
			<img src="{{media url='wysiwyg/porter_brand_4.png'}}" alt="">
		</a>
	</div>
</div>
<hr class="indent-40 white-space">
