<?php

/**
 * Grid Admin Cagegory Map Record Save Controller.
 * @category  Herfox
 * @package   Herfox_Matriz
 * @author    Herfox
 * @copyright Copyright (c) 2010-2016 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Matriz\Controller\Adminhtml\Submodelo;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Herfox\Matriz\Model\SubmodeloFactory
     */
    var $matrizFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Herfox\Matriz\Model\SubmodeloFactory $submodeloFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Herfox\Matriz\Model\SubmodeloFactory $submodeloFactory
    ) {
        parent::__construct($context);
        $this->submodeloFactory = $submodeloFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('matrizgrid/submodelo/addrow');
            return;
        }
        try {
            $rowData = $this->submodeloFactory->create();
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setEntityId($data['id']);
            }
            $rowData->save();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('matrizgrid/submodelo/index');
        
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Matriz::save');
    }
}
