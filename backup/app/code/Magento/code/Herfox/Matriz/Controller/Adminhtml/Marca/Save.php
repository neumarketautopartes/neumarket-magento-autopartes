<?php

/**
 * Grid Admin Cagegory Map Record Save Controller.
 * @category  Herfox
 * @package   Herfox_Matriz
 * @author    Herfox
 * @copyright Copyright (c) 2010-2016 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Matriz\Controller\Adminhtml\Marca;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Herfox\Matriz\Model\MarcaFactory
     */
    var $matrizFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Herfox\Matriz\Model\MarcaFactory $marcaFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Herfox\Matriz\Model\MarcaFactory $marcaFactory
    ) {
        parent::__construct($context);
        $this->marcaFactory = $marcaFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('matrizgrid/marca/addrow');
            return;
        }
        try {
            $rowData = $this->marcaFactory->create();
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setEntityId($data['id']);
            }
            $rowData->save();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('matrizgrid/marca/index');
        
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Matriz::save');
    }
}
