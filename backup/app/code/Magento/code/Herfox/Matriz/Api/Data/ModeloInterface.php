<?php
/**
 * Rules ModeloInterface.
 * @category  Herfox
 * @package   Herfox_Matriz
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */

namespace Herfox\Matriz\Api\Data;

interface ModeloInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const ID_MARCA = 'id_marca';
    const NOMBRE = 'nombre';
    const IS_ACTIVE = 'is_active';
    const UPDATE_TIME = 'update_time';
    const CREATED_AT = 'created_at';

   /**
    * Get EntityId.
    *
    * @return int
    */
    public function getEntityId();

   /**
    * Set EntityId.
    */
    public function setEntityId($entityId);

    /**
    * Get IdMarca.
    *
    * @return int
    */
    public function getIdMarca();

   /**
    * Set IdMarca.
    */
    public function setIdMarca($idMarca);

   /**
    * Get Nombre.
    *
    * @return varchar
    */
    public function getNombre();

   /**
    * Set Nombre.
    */
    public function setNombre($nombre);

   /**
    * Get IsActive.
    *
    * @return varchar
    */
    public function getIsActive();

   /**
    * Set StartingPrice.
    */
    public function setIsActive($isActive);

   /**
    * Get UpdateTime.
    *
    * @return varchar
    */
    public function getUpdateTime();

   /**
    * Set UpdateTime.
    */
    public function setUpdateTime($updateTime);

   /**
    * Get CreatedAt.
    *
    * @return varchar
    */
    public function getCreatedAt();

   /**
    * Set CreatedAt.
    */
    public function setCreatedAt($createdAt);
}
