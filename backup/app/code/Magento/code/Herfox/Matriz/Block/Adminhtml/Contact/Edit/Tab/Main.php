<?php
namespace Herfox\Matriz\Block\Adminhtml\Contact\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
    * @var \Herfox\Matriz\Helper\Data $helper
    */
    protected $helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Herfox\Matriz\Model\Years $options,
     * @param \Herfox\Matriz\Model\Modelosdata $modelos,
     * @param \Herfox\Matriz\Model\Marcasdata $marcas,
     * @param \Herfox\Matriz\Model\Submodelosdata $submodelos,
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Herfox\Matriz\Helper\Data $helper,
        \Herfox\Matriz\Model\Years $options,
        \Herfox\Matriz\Model\Modelosdata $modelos,
        \Herfox\Matriz\Model\Marcasdata $marcas,
        \Herfox\Matriz\Model\Submodelosdata $submodelos,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->_options = $options;
        $this->_modelos = $modelos;
        $this->_submodelos = $submodelos;
        $this->_marcas = $marcas;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Herfox\Matriz\Model\Contact */
        $model = $this->_coreRegistry->registry('ws_contact');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('contact_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Information')]);

        if ($model->getId()) {
            $fieldset->addField('reg_id', 'hidden', ['name' => 'reg_id']);
        }

        $fieldset->addField(
            'id_marca',
            'select',
            [
                'name' => 'id_marca',
                'label' => __('Marca'),
                'id' => 'id_marca',
                'class' => 'required-entry',
                'values' => $this->_marcas->getOptionArray(),
                'required' => true,
                'title' => __('Marca'),
                ]
            );
            $fieldset->addField(
                'id_modelo',
                'select',
                [
                    'name' => 'id_modelo',
                    'label' => __('Modelo'),
                    'id' => 'id_modelo',
                    'title' => __('Modelo'),
                    'values' => $this->_modelos->getOptionArray(),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
                $fieldset->addField(
                    'id_submodelo',
                    'select',
                    [
                        'name' => 'id_submodelo',
                        'label' => __('Submodelo'),
                        'id' => 'id_submodelo',
                        'title' => __('Submodelo'),
                        'class' => 'required-entry',
                        'values' => $this->_submodelos->getOptionArray(),
                        'required' => true,
                        ]
                    );
                $fieldset->addField(
                    'year',
                    'select',
                    [
                        'name' => 'year',
                        'label' => __('Year'),
                        'id' => 'year',
                        'title' => __('Year'),
                        'class' => 'required-entry',
                        'values' => $this->_options->getOptionArray(),
                        'required' => true,
                        ]
                    );
                    

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
