<?php
namespace Herfox\Matriz\Model\ResourceModel\Contact;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'reg_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Herfox\Matriz\Model\Contact', 'Herfox\Matriz\Model\ResourceModel\Contact');
    }
}
