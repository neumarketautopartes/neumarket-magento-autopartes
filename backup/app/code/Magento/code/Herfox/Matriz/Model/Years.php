<?php
/**
 * Herfox_Matriz Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Matriz\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Years implements OptionSourceInterface
{
    /**
     * Get Matriz row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $options = [
            "1980" =>__("1980"),
            "1981" =>__("1981"),
            "1982" =>__("1982"),
            "1983" =>__("1983"),
            "1984" =>__("1984"),
            "1985" =>__("1985"),
            "1986" =>__("1986"),
            "1987" =>__("1987"),
            "1988" =>__("1988"),
            "1989" =>__("1989"),
            "1990" =>__("1990"),
            "1991" =>__("1991"),
            "1992" =>__("1992"),
            "1993" =>__("1993"),
            "1994" =>__("1994"),
            "1995" =>__("1995"),
            "1996" =>__("1996"),
            "1997" =>__("1997"),
            "1998" =>__("1998"),
            "1999" =>__("1999"),
            "2000" =>__("2000"),
            "2001" =>__("2001"),
            "2002" =>__("2002"),
            "2003" =>__("2003"),
            "2004" =>__("2004"),
            "2005" =>__("2005"),
            "2006" =>__("2006"),
            "2007" =>__("2007"),
            "2008" =>__("2008"),
            "2009" =>__("2009"),
            "2010" =>__("2010"),
            "2011" =>__("2011"),
            "2012" =>__("2012"),
            "2013" =>__("2013"),
            "2014" =>__("2014"),
            "2015" =>__("2015"),
            "2016" =>__("2016"),
            "2017" =>__("2017"),
            "2018" =>__("2018"),
            "2019" =>__("2019"),
            "2020" =>__("2020"),
            "2021" =>__("2021"),
            "2022" =>__("2022"),
            "2023" =>__("2023"),
            "2024" =>__("2024"),
            "2025" =>__("2025"),
            "2026" =>__("2026"),
            "2027" =>__("2027"),
            "2028" =>__("2028"),
            "2029" =>__("2029"),
            "2030" =>__("2030"),
            "2031" =>__("2031"),
            "2032" =>__("2032"),
            "2033" =>__("2033"),
            "2034" =>__("2034"),
            "2035" =>__("2035"),
            "2036" =>__("2036"),
            "2037" =>__("2037"),
            "2038" =>__("2038"),
            "2039" =>__("2039"),
            "2040" =>__("2040")            
    ];
        return $options;
    }

    /**
     * Get Matriz row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Matriz row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
