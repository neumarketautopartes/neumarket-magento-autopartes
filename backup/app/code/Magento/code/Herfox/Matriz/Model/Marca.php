<?php

/**
 * Grid Marca Model.
 * @category  Herfox
 * @package   Herfox_Matriz
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Matriz\Model;

use Herfox\Matriz\Api\Data\MarcaInterface;

class Marca extends \Magento\Framework\Model\AbstractModel implements MarcaInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'marcas';

    /**
     * @var string
     */
    protected $_cacheTag = 'marcas';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marcas';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Herfox\Matriz\Model\ResourceModel\Marca');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Nombre.
     *
     * @return Nombre
     */
    public function getNombre()
    {
        return $this->getData(self::NOMBRE);
    }

    /**
     * Set Nombre.
     */
    public function setNombre($nombre)
    {
        return $this->setData(self::NOMBRE, $nombre);
    }
   

    /**
     * Set IsActive.
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
