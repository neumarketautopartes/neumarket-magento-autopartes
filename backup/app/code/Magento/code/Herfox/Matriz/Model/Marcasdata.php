<?php
/**
 * Herfox_Matriz Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Matriz\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Marcasdata implements OptionSourceInterface
{
    /**
     * Get Matriz row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_marcas.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        /** ------------------------------- */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('mg2_marcas'); 
        /** ------------------------------- */
        $sqlShippig = "SELECT * FROM " . $tableName ." Where is_active='1' ";
        $resultShippig = $connection->fetchAll($sqlShippig);
        foreach($resultShippig as $key => $value){
            $options[$value['entity_id']]= $value['nombre'];
        }
        $logger->info(print_r($options,true));
       
        $marcas = [
            $options
        ];
        return $options;
    }

    /**
     * Get Matriz row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Matriz row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
