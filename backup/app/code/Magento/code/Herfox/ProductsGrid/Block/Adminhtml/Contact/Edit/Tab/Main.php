<?php
namespace Herfox\ProductsGrid\Block\Adminhtml\Contact\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
    * @var \Herfox\ProductsGrid\Helper\Data $helper
    */
    protected $helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Herfox\ProductsGrid\Model\Status $options,
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Herfox\ProductsGrid\Helper\Data $helper,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Herfox\ProductsGrid\Model\Status $options,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->_options = $options;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Herfox\ProductsGrid\Model\Contact */
        $model = $this->_coreRegistry->registry('ws_contact');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('contact_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Información del producto')]);

        if ($model->getId()) {
            $fieldset->addField('webscraper_id', 'hidden', ['name' => 'webscraper_id']);
        }
        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Nombre Origal'),
                'id' => 'title',
                'readonly' => 'readonly',
                 'class' => 'traslate-text-hfx',
                'title' => __('Nombre Origal'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_title',
                'text',
                [
                    'name' => 'new_title',
                    'label' => __('Nombre'),
                    'id' => 'new_title',
                    'title' => __('Nombre'),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
                /** Nombre producto Fin */
                /** Part Number Inicio */
                $fieldset->addField(
                    'part_number',
                    'text',
                    [
                        'name' => 'part_number',
                        'label' => __('Part Number Origal'),
                        'id' => 'part_number',
                        'readonly' => 'readonly',
                        'class' => 'copy-data',
                        'title' => __('part Number Origal'),
                        'style' => 'background-color: #dcdcdc;',
                        ]
                    );
                    $fieldset->addField(
                        'new_part_number',
                        'text',
                        [
                            'name' => 'new_part_number',
                            'label' => __('Part Number'),
                            'id' => 'new_part_number',
                            'title' => __('Part Number'),
                            'class' => 'required-entry',
                            'required' => true,
                            ]
                        );
                /** Part Number Fin */
                /** Fabricante Inicio */
                $fieldset->addField(
                    'manufacturer',
                    'text',
                    [
                        'name' => 'manufacturer',
                        'label' => __('Fabricante Original'),
                        'id' => 'manufacturer',
                        'readonly' => 'readonly',
                        'class' => 'copy-data',
                        'title' => __('Fabricante Original'),
                        'style' => 'background-color: #dcdcdc;',
                        ]
                    );
                    $fieldset->addField(
                        'new_manufacturer',
                        'text',
                        [
                            'name' => 'new_manufacturer',
                            'label' => __('Fabricante'),
                            'id' => 'new_manufacturer',
                            'title' => __('Fabricante'),
                            'class' => 'required-entry',
                            'required' => true,
                            ]
                        );
                        $fieldset->addField(
                            'aply_to_manufaturer',
                            'checkbox',
                            [
                                'name' => 'aply_to_manufaturer',
                                'label' => __('Aplicar a todos'),
                                'id' => 'aply_to_manufaturer',
                                'title' => __('Aplicar a todos'),
                                ]
                            );
                /** Fabricante Fin */
                /** Precio Inicio */
                $fieldset->addField(
                    'price',
                    'text',
                    [
                        'name' => 'price',
                        'label' => __('Precio Original'),
                        'id' => 'price',
                        'class' => 'copy-data',
                        'readonly' => 'readonly',
                        'title' => __('Precio Original'),
                        'style' => 'background-color: #dcdcdc;',
                        ]
                    );
                    $fieldset->addField(
                        'new_price',
                        'text',
                        [
                            'name' => 'new_price',
                            'label' => __('Precio'),
                            'id' => 'new_price',
                            'title' => __('Precio'),
                            'class' => 'required-entry',
                            'required' => true,
                            ]
                        );
                /** Precio Final */
                /** Inventario Inicio */
                $fieldset->addField(
                    'stock',
                    'text',
                    [
                        'name' => 'stock',
                        'label' => __('Stock Original'),
                        'id' => 'stock',
                        'readonly' => 'readonly',
                        'class' => 'copy-data',
                        'title' => __('Stock Original'),
                        'style' => 'background-color: #dcdcdc;',
                        ]
                    );
                    $fieldset->addField(
                        'new_stock',
                        'text',
                        [
                            'name' => 'new_stock',
                            'label' => __('Stock'),
                            'id' => 'new_stock',
                            'title' => __('Stock'),
                            'class' => 'required-entry',
                            'required' => true,
                            ]
                        );
                /** Inventario Fin */
                /** Envio Inicio */
                $fieldset->addField(
                    'shipping',
                    'text',
                    [
                        'name' => 'shipping',
                        'label' => __('Envio Original'),
                        'id' => 'shipping',
                        'readonly' => 'readonly',
                        'class' => 'traslate-text-hfx',
                        'title' => __('Envio Original'),
                        'style' => 'background-color: #dcdcdc;',
                        ]
                    );
                    $fieldset->addField(
                        'new_shipping',
                        'text',
                        [
                            'name' => 'new_shipping',
                            'label' => __('Envio'),
                            'id' => 'new_shipping',
                            'title' => __('Envio'),
                            'class' => 'required-entry',
                            'required' => true,
                            ]
                        );
                        $fieldset->addField(
                            'aply_to_shippig',
                            'checkbox',
                            [
                                'name' => 'aply_to_shippig',
                                'label' => __('Aplicar a todos'),
                                'id' => 'aply_to_shippig',
                                'title' => __('Aplicar a todos'),
                                ]
                            );
                /** Envio Fin */
                $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
                /** Short Description Inicio */
                $fieldset->addField(
                    'short_description',
                    'textarea',
                    [
                        'name' => 'short_description',
                        'label' => __('Short description Original'),
                        'style' => 'height:12em;',
                        'class' => 'traslate-text-hfx',
                        'data-type' => 'editor',
                        'readonly' => 'readonly',
                        'style' => 'background-color: #dcdcdc;',
                    ]
                );
                $fieldset->addField(
                    'new_short_description',
                    'editor',
                    [
                        'name' => 'new_short_description',
                        'label' => __('Short description'),
                        'style' => 'height:12em; width:800px;',
                        'required' => true,
                        'config' => $wysiwygConfig
                    ]
                );
                /** Short Description Fin */
                /** Long Description Inicio */
                $fieldset->addField(
                    'long_description',
                    'textarea',
                    [
                        'name' => 'long_description',
                        'label' => __('Long description Original'),
                        'style' => 'height:12em;',
                        'class' => 'traslate-text-hfx',
                        'data-type' => 'editor',
                        'readonly' => 'readonly',
                        'style' => 'background-color: #dcdcdc;',
                    ]
                );
                $fieldset->addField(
                    'new_long_description',
                    'editor',
                    [
                        'name' => 'new_long_description',
                        'label' => __('Long description'),
                        'style' => 'height:12em; width:800px;',
                        'required' => true,
                        'config' => $wysiwygConfig
                    ]
                );
                /** Long Description Fin */
                /** Features Inicio */
                $fieldset->addField(
                    'features',
                    'textarea',
                    [
                        'name' => 'features',
                        'label' => __('Features Original'),
                        'class' => 'traslate-text-hfx',
                        'data-type' => 'editor',
                        'style' => 'height:12em;',
                        'readonly' => 'readonly',
                        'style' => 'background-color: #dcdcdc;',
                    ]
                );
                $fieldset->addField(
                    'new_features',
                    'editor',
                    [
                        'name' => 'new_features',
                        'label' => __('Features'),
                        'style' => 'height:12em; width:800px;',
                        'required' => true,
                        'config' => $wysiwygConfig
                    ]
                );
                /** Features Fin */
                
        
                $fieldset->addField(
                    'is_active',
                    'select',
                    [
                        'name' => 'is_active',
                        'label' => __('Status'),
                        'id' => 'is_active',
                        'title' => __('Status'),
                        'values' => $this->_options->getOptionArray(),
                        'class' => 'status',
                        'required' => true,
                    ]
                );
                $fieldset->addField(
                    'image_url',
                    'hidden',
                    [
                        'name' => 'image_url',
                        'id' => 'image_url',
                    ]
                );
                //$traslate = $this->helper->traslateText();
                $url =  $this->getUrl('Herfox/Grid/index');
                $Lastfield = $form->getElement('is_active');
                $Lastfield->setAfterElementHtml(
                        '<script type="text/javascript"> 
                        require(["jquery", "jquery/ui"], function($){ 
                            $(document).ready(function () {
                                var idCopy = "";
                                var idImput = "";
                                if($("#wpgrid_is_active").val()!="2"){
                                $( ".copy-data" ).each(function( index,value ) {
                                    idCopy= $(this).attr("id").split("wpgrid_");
                                    if($("#wpgrid_new_"+idCopy[1]+"").val()==""){
                                        $("#wpgrid_new_"+idCopy[1]+"").val($(this).val());
                                    }
                                  });
                                }
                            var fields = "";
                             $( ".traslate-text-hfx" ).each(function( index,value ) {
                                fields += $(this).attr("id")+"@&nbsp;@"+$(this).val()+"@&nbsp;@"+$(this).data("type")+"#&nbsp;#"; 
                              });
                              $("#msgLoaderHf").html("Traduciendo, por favor espere");
                              $(".loading-mask-HF").show();
                                $.ajax({
                                          method: "POST",
                                          url: "http://ec2-18-205-249-52.compute-1.amazonaws.com/wsproductsgrid",
                                          data: { traducir: fields},
                                          dataType: "json"
                                        })
                                      .done(function( msg ) {
                                        $("#togglewpgrid_new_short_description").trigger("click");
                                        $("#togglewpgrid_new_long_description").trigger("click");
                                        $("#togglewpgrid_new_features").trigger("click");
                                        $(msg).each(function( index,value ) {
                                            if($("#wpgrid_new_"+value[0]+"").val()==""){
                                                $("#wpgrid_new_"+value[0]+"").val(value[1]);
                                            }
                                          });
                                          $("#msgLoaderHf").html("Traducción completa");
                                          $(".loading-mask-HF").hide();
                                        //do something with you return data 
                                      });
                                    /** Save in magento function inicio */
                                    
                                    $("#saveinmagento").click(function(){
        
                                            if ($("#edit_form").valid()) {
                                                var params = $("#edit_form").serializeArray();
                                                $("#msgLoaderHf").html("Creando producto, por favor espere");
                                                $(".loading-mask-HF").show();
                                                $.ajax({
                                                    method: "POST",
                                                    url: "http://ec2-18-205-249-52.compute-1.amazonaws.com/grid/index/product/",
                                                    data: { data: params},
                                                    dataType: "json"
                                                  })
                                                .done(function( msg ) {
                                                    $(msg).each(function( index,value ) {
                                                        console.log(value);
                                                  });
                                                  $("#msgLoaderHf").html("Producto creado correctamente");
                                                  window.location.href = "http://ec2-18-205-249-52.compute-1.amazonaws.com/admin_autoparts/grid/grid/index/";
                                                });
                                                return false;
                                            }else{
                                                return false;
                                            }
                                        });
                                    /** Save in magento function Fin */
                        });
                        });
                        </script>
                        <div class="loading-mask loading-mask-HF" data-role="loader">
                        <div class="loader">
                            <p id="msgLoaderHf" ></p>
                            </div>
                        </div>'
                );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
