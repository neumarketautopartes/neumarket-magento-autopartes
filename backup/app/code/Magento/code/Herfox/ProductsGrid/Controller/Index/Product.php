<?php
 
namespace Herfox\ProductsGrid\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use \Statickidz\GoogleTranslate;
 
class Product extends \Magento\Framework\App\Action\Action
{
    private $_save;

/**
 * CustomClass constructor.
 *
 * @param \Magento\Framework\App\Action\Context               $context
 * @param \Custom\Module\Controller\Adminhtml\UpdateData\Save $save
 */
public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Herfox\ProductsGrid\Controller\Adminhtml\Contacts\Save $save
) {
    parent::__construct($context);
    $this->_save = $save;
}

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('\Herfox\ProductsGrid\Model\Product');
        $dataFinal = array();
        foreach($_POST['data'] as $key => $value){
                $dataFinal[$value['name']] = $value['value'];
        }
        /*echo '<pre>';
        print_r($dataFinal);
        echo '</pre>';*/
        $resulSave = $this->_save->execute($dataFinal);
        if($resulSave='success'){
            $data =  $model->createProduct($dataFinal);
        }
       echo json_encode('completado');
    }
}