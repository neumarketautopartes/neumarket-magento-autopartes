<?php
namespace Herfox\ProductsGrid\Model\ResourceModel\Contact;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'webscraper_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Herfox\ProductsGrid\Model\Contact', 'Herfox\ProductsGrid\Model\ResourceModel\Contact');
    }
}
