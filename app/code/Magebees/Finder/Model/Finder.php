<?php
namespace Magebees\Finder\Model;

class Finder extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magebees\Finder\Model\ResourceModel\Finder');
    }

    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magebees\Finder\Model\DropdownsFactory $dropdownsFactory,
        \Magebees\Finder\Model\YmmvalueFactory $ymmvalueFactory,
        \Magebees\Finder\Model\MapvalueFactory $mapvalueFactory,
        \Magebees\Finder\Helper\Data $finderHelper,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_dropdownsFactory = $dropdownsFactory;
        $this->_ymmvalueFactory = $ymmvalueFactory;
        $this->_mapvalueFactory = $mapvalueFactory;
        $this->_catalogSession = $catalogSession;
        $this->_finderHelper = $finderHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    
    public function getDropdowns($finderId)
    {
        $collection = $this->_dropdownsFactory->create()->getCollection()
            ->addFieldToFilter('finder_id', $finderId);
        return $collection;
    }
    
    public function isDeleteYmmData($ymm_value_id)
    {
        $mapCollection = $this->_mapvalueFactory->create()->getCollection()->addFieldToFilter('ymm_value_id', $ymm_value_id);
        $count =  $mapCollection->getSize();
        
        if ($count) {
            return false;
        }
    
        $ymmCollection = $this->_ymmvalueFactory->create()->getCollection()->addFieldToFilter('parent_id', $ymm_value_id);
        
        $mcount =  $ymmCollection->getSize();
        if ($mcount) {
            return false;
        }
        return true;
    }
    
    
    public function saveDropDownValues($dropdowns)
    {
        $session = $this->_catalogSession;
        $findername    = 'mbfinder_' . $this->getId();

        if (!$dropdowns) {
            return false;
        }
            
        if (!is_array($dropdowns)) {
            return false;
        }
         
        $values = [];
        $id      = 0;
        $current = 0;
        foreach ($this->getDropdowns($this->getId()) as $d) {
            $id = $d->getId();
            $values[$id] = isset($dropdowns[$id]) ? $dropdowns[$id] : 0;
            if (isset($dropdowns[$id]) && ($dropdowns[$id])) {
                $current = $dropdowns[$id];
            }
        }
        
        if ($id) {
            $values['last']    = $values[$id];
            $values['current'] = $current;
        }
                   
        $session->setData($findername, $values);
        
        return true;
    }
    
    public function getSavedValue($dropdownId)
    {
        $session = $this->_catalogSession;
        $name    = 'mbfinder_' . $this->getId();
                
        $values = $session->getData($name);
        
        if (!is_array($values)) {
            return 0;
        }
            
        if (empty($values[$dropdownId])) {
            return 0;
        }
            
        return $values[$dropdownId];
    }
    
    public function getYmmValueFromPath($param)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper-finder-model.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        //for compatible with layered navigation
        $res ="";
        $param = trim($param, "/");

            $param = explode("-", $param);
            $categories =  $this->_finderHelper->getCategories();
            $search = false;
            foreach($categories as $key => $value){
                if($param[0]==$value){
                    $search = true;
                    break;
                }
            }
            if($search){
                $ymm_param = $param;
                $res = $ymm_param[count($ymm_param)-1];
            }
            $logger->info('this is the res: '.$res);
            return $res;
    }
    
    public function getDropdownsByCurrent($current)
    {
        $dropdowns = [];
        while ($current) {
            $valueModel = $this->_ymmvalueFactory->create()->load($current);
            $dropdowns[$valueModel->getDropdownId()]= $current;
            $current = $valueModel->getParentId();
        }
        return $dropdowns;
    }
}
