<?php
namespace Magebees\Finder\Controller\Index;

use \Magento\Framework\App\Action\Action;

class Search extends Action
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magebees\Finder\Helper\Data $finderHelper
    ) {
        parent::__construct($context);
        $this->_url = $context->getUrl();
        $this->_finderHelper = $finderHelper;
    }
    
    public function execute()
    {
        /*echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        die();*/
        $finderid = $this->getRequest()->getParam('finder_id');
        if ($this->getRequest()->getParam('reset')) {
             $session = $this->_objectManager->create('Magento\Catalog\Model\Session');
            
            $findername    = 'mbfinder_' . $finderid;
            $session->setData($findername, "");
            if($finderid==3){
                $result_page_url =  $this->_url->getUrl('llantas').$finderid;
            }else{
                $result_page_url =  $this->_url->getUrl('frenos').$finderid;
            }
        } else {

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper-searc-controller.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        
        $finderModel = $this->_objectManager->create('Magebees\Finder\Model\Finder')->setId($finderid);
        $dropdowns = $this->getRequest()->getParam('finder');
        $logger->info('Finder: '.print_r($dropdowns,true));
            if ($dropdowns) {
                $finderModel->saveDropDownValues($dropdowns);
            }
            if($finderid==3){
                $result_page_url =  $this->_url->getUrl('llantas-'.$this->getResultPageUrl($finderid));
            }else{
                $result_page_url =  $this->_url->getUrl('frenos-'.$this->getResultPageUrl($finderid));
            }
                $result_page_url =  $result_page_url;
        }
        $this->getResponse()->setRedirect($result_page_url);
    }
    
    public function getResultPageUrl($finderid)
    {
        $urlParam = '';
        //$separator = Mage::getStoreConfig('finder_section/finder_group/separator');
        $separator = "-";
         $session = $this->_objectManager->create('Magento\Catalog\Model\Session');
        $name    = 'mbfinder_' . $finderid;
        
        $values = $session->getData($name);
        if (!is_array($values)) {
            $values = [];
        }
        
        foreach ($values as $key => $value) {
            if ('current' == $key) {
                $urlParam .= $value;
                break;
            }
           
            if (!empty($value) && is_numeric($key)) {
                $valueModel = $this->_objectManager->create('Magebees\Finder\Model\Ymmvalue')->load($value);
                if ($valueModel->getId()) {
                    $urlParam .= strtolower(preg_replace('/[^\da-zA-Z]/', $separator, $valueModel->getValue())).$separator;
                }
            }
        }
        return $urlParam;
    }
}
