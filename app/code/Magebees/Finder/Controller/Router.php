<?php
namespace Magebees\Finder\Controller;

/**
 * Finder Custom router Controller Router
 *
 */
class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * @param \Magento\Framework\App\ActionFactory $actionFactory
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magebees\Finder\Helper\Data $finderHelper
    ) {
        $this->actionFactory = $actionFactory;
        $this->_finderHelper = $finderHelper;
    }
    
    /**
     * Validate and Match
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/finder_router.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $pageId = trim($request->getPathInfo(), '/');
        $categories =  $this->_finderHelper->getCategories();
        $search = false;
        foreach($categories as $key => $value){
            if(strpos($pageId, $value)!==false){
                $search = true;
                break;
            }
        }
        $logger->info(print_r($categories,true));
        if ($search) { // for rewrite custom finder page
            $request->setModuleName('finder')
                    ->setControllerName('index')
                    ->setActionName('index');
            $request->setAlias(\Magento\Framework\UrlInterface::REWRITE_REQUEST_PATH_ALIAS, $pageId);
            return $this->actionFactory->create(
                'Magento\Framework\App\Action\Forward',
                ['request' => $request]
            );
        } else {
            return false;
        }
    }
}
