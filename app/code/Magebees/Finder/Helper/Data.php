<?php
namespace Magebees\Finder\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_categoryCollectionFactory;
    protected $_categoryHelper;
  
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
         \Magento\Catalog\Helper\Category $categoryHelper

    ) {
        $this->request = $request;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        parent::__construct($context);
        
    }
    public function getCategories(){
        $categories = $this->_categoryCollectionFactory->create()                              
        ->addAttributeToSelect('*')
        ->setStore(1);
        foreach ($categories as $category){
            $catUrlKey = $category->getUrlKey();
            if($category->getUrlKey()=='default-category'){
                $catUrlKey = 'autopartes';
            }
                if($category->getUrlKey()!='root_catalog'){
                 $options[] = str_replace('-','_',$catUrlKey);
                }
       }
       $options[] = 'llantas';
       $options[] = 'frenos';
       return $options;
    }
    
    public function getFinderDataForTab($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $finder = $objectManager->create('Magebees\Finder\Model\Finder');
        
        $finder->load($id);
        return $finder;

    }

    public function getFinderId($path)
    {
        $finderparams = [];
        $path = trim($path, '/');
        $finderparams = explode('/', $path);
        if (array_key_exists(1, $finderparams)) {
            $finderId = $finderparams[1];
        } else {
            return 0;
        }
        return $finderId;
    }
    
    public function resetFinder($path)
    {
        $finderparams = [];
        $path = trim($path, '/');
        $param = explode("-", $path);
        $categories =  $this->getCategories();
        $search = false;
        foreach($categories as $key => $value){
            if($param[0]==$value){
                $search = true;
                break;
            }
        }
            return $search;
    }
}
