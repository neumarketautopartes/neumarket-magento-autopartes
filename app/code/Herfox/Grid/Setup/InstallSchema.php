<?php
/**
 * Grid Schema Setup.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2016 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */

namespace Herfox\Grid\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        /*
         * Create table 'webscraper_products'
         */

        $table = $installer->getConnection()->newTable(
            $installer->getTable('webscraper_products')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Grid Record Id'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Nombre Prodcuto'
        )
        /** Columna de edicion Nombre */
        ->addColumn(
            'new_title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Nombre Prodcuto'
        )
        /** Columna de edicion Nombre */
        ->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
                'unsigned' => true,
                'unique' => true
            ],
            'Product Id'
        )
        ->addColumn(
            'carid_item_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [
                'nullable' => false,
                'unsigned' => true,
            ],
            '64k',
            'Carid Item Number'
        )
        ->addColumn(
            'ref_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
                'unsigned' => true,
            ],
            'Referencia Id'
        )
        ->addColumn(
            'long_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            [],
            'Long Description'
        )
        /** Campo de edicion Long Description  */
        ->addColumn(
            'new_long_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            [],
            'Long Description'
        )
        /** Campo de edicion Long Description  */
        ->addColumn(
            'urk',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Url'
        )
        ->addColumn(
            'oe_numbers',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'OE Numbers'
        )
        ->addColumn(
            'features',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            [],
            'Features'
        )
        /** Campo de edición Features */
        ->addColumn(
            'new_features',  
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            [],
            'Features'
        )
        /** Campo de edición Features */
        ->addColumn(
            'car_url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Car URL'
        )
        ->addColumn(
            'part_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Part Number'
        )
        /** Columna de edición Numero de parte */
        ->addColumn(
            'new_part_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Part Number'
        )
        /** Columna de edición Numero de parte */
        ->addColumn(
            'manufacturer',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Manufacturer'
        )
        ->addColumn(
            'new_manufacturer',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'New Manufacturer'
        )
        ->addColumn(
            'price',
            \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
            '64k',
            [],
            'Price'
        )
        /** Columna de edicion price */
        ->addColumn(
            'new_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
            '64k',
            [],
            'Price'
        )
        /** Columna de edicion price */
        
        ->addColumn(
            'stock',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Stock'
        )
        /** Columna de edicion price */
        /** Columna de edición stock */
         ->addColumn(
            'new_stock',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            '64k',
            [],
            'Stock'
        )
        /** Columna de edición stock */
        ->addColumn(
            'shipping',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Shipping'
        )
        /** Columna de edicion Encio */
        ->addColumn(
            'new_shipping',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Shipping'
        )
        /** Columna de edicion Encio */
        ->addColumn(
            'new_refactor',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'New Refactor'
        )
        ->addColumn(
            'created',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            255,
            [],
            'Creando en Magento'
        )
        ->addColumn(
            'cron',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            255,
            [],
            'Cron'
            )
        ->addColumn(
            'upc',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'UPC'
        )
        ->addColumn(
            'parent',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Parent'
        )
        ->addColumn(
            'image_url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Image Url'
        )
        ->addColumn(
            'short_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            ['nullable' => false],
            'Short Description'
        )
        /** Columna de edición Short Descripcion */
        ->addColumn(
            'new_short_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '128k',
            ['nullable' => false],
            'Short Description'
        )
        /** Columna de edición Short Descripcion */
        ->addColumn(
            'publish_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Publish Date'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'Active Status'
        )
        
        /** Columna Atributo Inicio */
        ->addColumn(
            'atributo',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => false],
            'Atributo'
        )
         ->addColumn(
            'new_atributo',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => false],
            'Nuevo Atributo'
        )
        /** Columna Atributo Fin */
        ->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
            ],
            'Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Modification Time'
        )->setComment(
            'Row Data Table'
        );

        $installer->getConnection()->createTable($table);
        /** --------------------------------------------------------------------- */
        $marca = $installer->getConnection()->newTable(
            $installer->getTable('rules')
        );
        $marca->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Record Id'
        );
        $marca->addColumn(
            'valor',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, ],
            'Valor'
        );
        $marca->addColumn(
            'regla',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, ],
            'Regla'
        );
        $marca->addColumn(
            'rule_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'Tipo de Regla'
        );
        $marca->addColumn(
            'tipo_dato',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'Tipo de Relación'
        );
        $marca->addColumn(
            'attribute_set',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Conjunto de atributos '
        );
        $marca->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            [],
            'Active Status'
        );
        $marca->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
            ],
            'Creation Time'
        );
        $marca->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Update Time'
        );
        $installer->getConnection()->createTable($marca);
        /** ---------------------------------------------------- */
        if (!$installer->tableExists('webscraper_attributes')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_attributes'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'nombre',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'new_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            );
            $installer->getConnection()->createTable($table);
        }
        /** ---------------------------------------------------- */
        if (!$installer->tableExists('webscraper_attributes_options')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_attributes_options'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'nombre',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'new_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Id Atributo'
            );
            $installer->getConnection()->createTable($table);
        }
         /** ---------------------------------------------------- */
         if (!$installer->tableExists('webscraper_attributes_rel')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_attributes_rel'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Atributo'
            )->addColumn(
                'option_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Opcion'
            )->addColumn(
                'part_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Producto'
            );
            $installer->getConnection()->createTable($table);
        }
        /** ---------------------------------------------------- */
        if (!$installer->tableExists('webscraper_brands')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_brands'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'marca',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'modelo',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Modelo'
            )->addColumn(
                'submodelo',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Submodelo'
            )->addColumn(
                'year',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Año'
            )->addColumn(
                'part_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Producto'
            );
            $installer->getConnection()->createTable($table);
        }
        /** ---------------------------------------------------- */
        if (!$installer->tableExists('webscraper_categories')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_categories'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'nombre',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )->addColumn(
                'new_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Nombre'
            )
            ->addColumn(
                'parent',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Categoria padre'
            );
            $installer->getConnection()->createTable($table);
        }
        /** ---------------------------------------------------- */ 

        if (!$installer->tableExists('webscraper_categories_rel')) {
            $table = $installer->getConnection()
            ->newTable($installer->getTable('webscraper_categories_rel'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
            )->addColumn(
                'id_category',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Id Categoria'
            )->addColumn(
                'part_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Numero de parte'
            );
            $installer->getConnection()->createTable($table);
        }

        /** ---------------------------------------------------- */ 
        /**  Insertar datos de prueba  Inicio */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('webscraper_products');
        $arrayProduct = array(
            array(
			'1',
			'mpn12601',
			'Formula 100 Series OEM Semi-Metallic Brake Pads',
			'100.00020',
			'', 
			'24.88',
			'', 
			'',
			'Centric',
			'805890160267',
            'https://www.carid.com/images/centric/brake-parts/formula-100-series-oem-brake-pads.jpg',
            'Ford Cortina 1.5L Manufactured After October without Deluxe Package 1966, Formula 100 Series? OEM Semi-Metallic Front Disc Brake Pads by Centric®. Designed to perfectly fit your vehicle and meet/exceed the exacting standards for OE braking performance, these pads are a true OE formulation, manufactured using OE processes and OE materials by OE manufacturers. Like OE brake pads, these items are scorched eliminating the need for a break-in period.',
            '<div class="prod-full-descr" data-sitemap="description"><p>Designed to meet or even, exceed the exacting standards for OE braking performance, these <a href="/centric/">Centric</a> 100 Series brake pads are a true OE formulation, manufactured using OE materials and processes such as scorching and positive molding process. Scorching is the OE process that enhances key friction performance levels as it removes any uncured bonding agents eliminating the need for initial break-in and reducing noise caused by pad glazing. Scorching thermally conditions the pad material which yields a more consistent and higher friction level right out of the box. Scorching benefits the vehicle owner by promoting a more complete bed-in of new pads, increasing the effective stopping power from the first stop. </p>',
            '<ul><li>Application specific friction formula so your vehicle stops the way it was designed to stop</li><li>Designed to meet or exceed standards for OE braking performance</li><li>The positive molding process utilized by OE suppliers assures consistent material density</li><li>OE design ensures correct pad fitment in the caliper unit</li><li>OE style shims eliminate noise associated with pad vibration</li><li>Scorched to accelerate the break-in process</li><li>Sold as a set for two wheels</li></ul>',
            '',
            'https://www.carid.com/centric/formula-100-series-oem-semi-metallic-front-disc-brake-pads-mpn-100-00020.html?singleid=33996606&url=16827113',
            '1'
            ),
            array(
                '2',
                'mpn12602',
                'Formula 100 Series? OEM Semi-Metallic Rear Disc Brake Pads ',
                '100.00300',
                '',
                '17.86',
                '',
                '',
                'Centric',
                '805890160274',
                'https://www.carid.com/images/centric/brake-parts/formula-100-series-oem-brake-pads.jpg',
                'Formula 100 Series? OEM Semi-Metallic Rear Disc Brake Pads (100.00300) by Centric®. Designed to perfectly fit your vehicle and meet/exceed the exacting standards for OE braking performance, these pads are a true OE formulation, manufactured using OE processes and OE materials by OE manufacturers. Like OE brake pads, these items are scorched eliminating the need for a break-in period.',
                '<div class="prod-full-descr" data-sitemap="description"><p>Designed to meet or even, exceed the exacting standards for OE braking performance, these <a href="/centric/">Centric</a> 100 Series brake pads are a true OE formulation, manufactured using OE materials and processes such as scorching and positive molding process. Scorching is the OE process that enhances key friction performance levels as it removes any uncured bonding agents eliminating the need for initial break-in and reducing noise caused by pad glazing. Scorching thermally conditions the pad material which yields a more consistent and higher friction level right out of the box. Scorching benefits the vehicle owner by promoting a more complete bed-in of new pads, increasing the effective stopping power from the first stop. </p>',
                '<ul><li>Application specific friction formula so your vehicle stops the way it was designed to stop</li><li>Designed to meet or exceed standards for OE braking performance</li><li>The positive molding process utilized by OE suppliers assures consistent material density</li><li>OE design ensures correct pad fitment in the caliper unit</li><li>OE style shims eliminate noise associated with pad vibration</li><li>Scorched to accelerate the break-in process</li><li>Sold as a set for two wheels</li></ul>',
                '100.00020',
                'https://www.carid.com/centric/formula-100-series-oem-semi-metallic-rear-disc-brake-pads-mpn-100-00300.html',
                '1'
            ),
            array(
                '3',
                'mpn12603',
                'Formula 100 Series OEM Semi-Metallic Front Disc Brake Pads',
                '100.00310',
                '',
                '19.88',
                'In Stock (32)',
                'Ships within 24 hrs',
                'Centric',
                '805890160281',
                'https://www.carid.com/images/centric/brake-parts/formula-100-series-oem-brake-pads.jpg',
                'Formula 100 Series? OEM Semi-Metallic Front Disc Brake Pads (100.00310) by Centric®. Designed to perfectly fit your vehicle and meet/exceed the exacting standards for OE braking performance, these pads are a true OE formulation, manufactured using OE processes and OE materials by OE manufacturers. Like OE brake pads, these items are scorched eliminating the need for a break-in period.',
                '<div class="prod-full-descr" data-sitemap="description"><p>Designed to meet or even, exceed the exacting standards for OE braking performance, these <a href="/centric/">Centric</a> 100 Series brake pads are a true OE formulation, manufactured using OE materials and processes such as scorching and positive molding process. Scorching is the OE process that enhances key friction performance levels as it removes any uncured bonding agents eliminating the need for initial break-in and reducing noise caused by pad glazing. Scorching thermally conditions the pad material which yields a more consistent and higher friction level right out of the box. Scorching benefits the vehicle owner by promoting a more complete bed-in of new pads, increasing the effective stopping power from the first stop. </p>',
                '<ul><li>Application specific friction formula so your vehicle stops the way it was designed to stop</li><li>Designed to meet or exceed standards for OE braking performance</li><li>The positive molding process utilized by OE suppliers assures consistent material density</li><li>OE design ensures correct pad fitment in the caliper unit</li><li>OE style shims eliminate noise associated with pad vibration</li><li>Scorched to accelerate the break-in process</li><li>Sold as a set for two wheels</li></ul>',
                '100.00020',
                'https://www.carid.com/centric/formula-100-series-oem-semi-metallic-rear-disc-brake-pads-mpn-100-00310.html',
                '1'
            ),
            array(
                4,
                'mpn12604',
                'Formula 200 Series OEM Semi-Metallic Front Disc 2000 Brake Pads',
                '100.00450',
                '',
                '23.13',
                '',
                '',
                'Centric',
                '805890160328',
                'https://www.carid.com/images/centric/brake-parts/formula-100-series-oem-brake-pads.jpg805890160328',
                'Volkswagen Transporter Manufactured After August 1970, Formula 100 Series? OEM Semi-Metallic Front Disc Brake Pads by Centric®. Designed to perfectly fit your vehicle and meet/exceed the exacting standards for OE braking performance, these pads are a true OE formulation, manufactured using OE processes and OE materials by OE manufacturers. Like OE brake pads, these items are scorched eliminating the need for a break-in period.',
                '<div class="prod-full-descr" data-sitemap="description"><p>Designed to meet or even, exceed the exacting standards for OE braking performance, these <a href="/centric/">Centric</a> 100 Series brake pads are a true OE formulation, manufactured using OE materials and processes such as scorching and positive molding process. Scorching is the OE process that enhances key friction performance levels as it removes any uncured bonding agents eliminating the need for initial break-in and reducing noise caused by pad glazing. Scorching thermally conditions the pad material which yields a more consistent and higher friction level right out of the box. Scorching benefits the vehicle owner by promoting a more complete bed-in of new pads, increasing the effective stopping power from the first stop. </p>',
                '<ul><li>Application specific friction formula so your vehicle stops the way it was designed to stop</li><li>Designed to meet or exceed standards for OE braking performance</li><li>The positive molding process utilized by OE suppliers assures consistent material density</li><li>OE design ensures correct pad fitment in the caliper unit</li><li>OE style shims eliminate noise associated with pad vibration</li><li>Scorched to accelerate the break-in process</li><li>Sold as a set for two wheels</li></ul>',
                '',
                'https://www.carid.com/centric/formula-100-series-oem-semi-metallic-front-disc-brake-pads-mpn-100-00450.html?singleid=33998465&url=18633468',
                '1'
            ),
            array(
                5,
                'mpn12605',
                'C-Tek  Brake Pads',
                '102.00020',
                '',
                '8.28',
                'In Stock (18)',
                'Ships within 24 hrs',
                'Centric',
                '805890057031',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'C-Tek? Semi-Metallic Front Disc Brake Pads (102.00020) by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '',
                'https://www.carid.com/centric/c-tek-semi-metallic-front-disc-brake-pads-mpn-102-00020.html',
                '1'
            ),
            array(
                6,
                'mpn12607',
                'C-Tek Semi-Metallic Rear Disc Brake Pads',
                '102.00090',
                '',
                '10.83',
                'In Stock (36)',
                'Ships within 24 hrs',
                'Centric',
                '805890243892',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'C-Tek? Semi-Metallic Rear Disc Brake Pads (102.00090) by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '102.00020',
                'https://www.carid.com/centric/c-tek-semi-metallic-rear-disc-brake-pads-mpn-102-00090.html',
                '1'
            ),
            array(
                '7',
                'mpn12608',
                'C-Tek Semi-Metallic Front Disc Brake Pads',
                '102.00230',
                '',
                '14.43',
                'In Stock (66)',
                'Ships within 24 hrs',
                'Centric',
                '805890243960',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'C-Tek? Semi-Metallic Rear Disc Brake Pads (102.00230) by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '102.00020',
                'https://www.carid.com/centric/c-tek-semi-metallic-front-disc-brake-pads-mpn-102-00230.html',
                '1'
            ),
            array(
                '8',
                'mpn12609',
                'C-Tek Ceramic Brake Pads',
                '102.00270',
                '',
                '12.03',
                'In Stock (115)',
                'Ships within 24 hrs',
                'Centric',
                '805890243977',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'MG MGB with Banjo Type Rear Axle 1962, C-Tek? Semi-Metallic Front Disc Brake Pads by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '',
                'https://www.carid.com/centric/c-tek-semi-metallic-front-disc-brake-pads-mpn-102-00270.html?singleid=33996734&url=88099232',
                '1'
            ),
            array(
                '9',
                'mpn12610',
                'C-Tek Ceramic Fron Disc Brake Pads',
                '102.00300',
                '',
                '7.54',
                'In Stock (22)',
                'Ships within 24 hrs',
                'Centric',
                '805890243984',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'C-Tek? Semi-Metallic Rear Disc Brake Pads (102.00300) by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '102.00270',
                'https://www.carid.com/centric/c-tek-semi-metallic-rear-disc-brake-pads-mpn-102-00300.html',
                '1'
            ),
            array(
                '10',
                'mpn12611',
                'C-Tek Ceramic Rea Brake Pads',
                '102.00310',
                '',
                '6.34',
                'In Stock (155)',
                'Ships within 24 hrs',
                'Centric',
                '805890057048',
                'https://www.carid.com/images/centric/brake-parts/c-tek-metallic-brake-pads.jpg',
                'C-Tek? Semi-Metallic Front Disc Brake Pads (102.00310) by Centric®. Designed to provide consistent braking power without brake squealing, groaning, and vibration, these brake pads also produce minimal dust and prolong rotor life. 100% asbestos- and copper-free formulas make the products absolutely safe for environmentally friendly operation.',
                '<div class="prod-full-descr" data-sitemap="description"><p>These pads provide consistent, quiet, and <a href="/brakes.html">smooth braking performance</a>. They provide excellent brake pad and rotor life while producing minimal brake dust for cleaner tires and wheels. C-Tek metallic brake pads are formulated to reduce noise and come with shims to virtually eliminate noise and vibration when braking. </p>',
                '<ul><li>100% asbestos- and copper-free formulas</li><li>Semi-metallic compounds for excellent braking performance</li><li>Ultra-low dusting for cleaner wheels and tires</li><li>Shimmed to eliminate noise and vibration</li><li>Formulated for reduced noise</li><li>Scorched for easy bed-in</li><li>OE fit</li><li>Sold as a set for two wheels</li></ul>',
                '102.00270',
                'https://www.carid.com/centric/c-tek-semi-metallic-rear-disc-brake-pads-mpn-102-00310.html',
                '1'
            ),
        );
        foreach($arrayProduct as $key => $value){
            $sql = "INSERT INTO " . $tableName . " (
                product_id,
                carid_item_number,
                title,
                part_number,
                oe_numbers,
                price,
                stock,
                shipping,
                manufacturer,
                upc,
                image_url,
                short_description,
                long_description,
                features,
                parent,
                car_url,
                is_active
                  ) VALUES (
                '".$value[0]."',
                '".$value[1]."',
                '".$value[2]."',
                '".$value[3]."',
                '".$value[4]."',
                '".$value[5]."',
                '".$value[6]."',
                '".$value[7]."',
                '".$value[8]."',
                '".$value[9]."',
                '".$value[10]."',
                '".$value[11]."',
                '".$value[12]."',
                '".$value[13]."',
                '".$value[14]."',
                '".$value[15]."',
                '".$value[16]."'
                )";
                $connection->query($sql);
        }
        /** ----------------------------------------------------------- */
        $arrayBrands = array(
            array(
                '1',
                'Mazda',
                '3',
                '2018',
                '100.00020',
            ),
            array(
                '2',
                'Mazda',
                'Sedan',
                '2017',
                '100.00020',
            ),
            array(
                '3',
                'Renault',
                'Spectra',
                '2017',
                '100.00020',
            ),
            array(
                '4',
                'Jaguar',
                'F-Pace',
                '2019',
                '102.00020',
            ),
            array(
                '5',
                'Mazda',
                '3',
                '2017',
                '102.00020',
            ),
            array(
                '6',
                'Hyundai',
                'i30',
                '2016',
                '102.00020',
            ),
        );
        foreach($arrayBrands as $key => $value){
            $sql = "INSERT INTO webscraper_brands (
                id,
                marca,
                modelo,
                year,
                part_number
                  ) VALUES (
                '".$value[0]."',
                '".$value[1]."',
                '".$value[2]."',
                '".$value[3]."',
                '".$value[4]."'
                )";
                $connection->query($sql);
        }
        /** ----------------------------------------------------------- */
        $arrayAtributes = array(
            array(
                '1',
                '1',
                '2',
                '100.00300',
            ),
            array(
                '2',
                '1',
                '1',
                '100.00310',
            ),
            array(
                '3',
                '2',
                '5',
                '102.00090',
            ),
            array(
                '4',
                '1',
                '2',
                '102.00090',
            ),
            array(
                '5',
                '2',
                '5',
                '102.00230',
            ),
            array(
                '6',
                '1',
                '1',
                '102.00230',
            ),
        );
        foreach($arrayAtributes as $key => $value){
            $sql = "INSERT INTO webscraper_attributes_rel (
                id,
                attribute_id,
                option_id,
                part_number
                  ) VALUES (
                '".$value[0]."',
                '".$value[1]."',
                '".$value[2]."',
                '".$value[3]."'
                )";
                $connection->query($sql);
        }
        /** ----------------------------------------------------------- */
        $sqlAttr = "INSERT INTO webscraper_attributes (id,nombre) VALUES ('1', 'Brakes Location'), ('2', 'Friction Material')";
        $connection->query($sqlAttr);
        /** ----------------------------------------------------------- */
        $sqlOptios = "INSERT INTO webscraper_attributes_options (id,nombre,attribute_id) VALUES ('1', 'Front', '1'), ('2', 'Rear', '1'), ('3', 'Ceramic', '2'), ('4', 'Organic', '2'), ('5', 'Semi-Metallic', '2')";
        $connection->query($sqlOptios);
        /** ----------------------------------------------------------- */
        $sqlCat = "INSERT INTO webscraper_categories (id,nombre) VALUES ('1', 'Brake Parts')";
        /** ----------------------------------------------------------- */
        $installer->endSetup();
    }
}
