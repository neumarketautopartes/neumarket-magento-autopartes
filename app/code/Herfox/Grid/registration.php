<?php
/**
 * Grid Module registration.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Herfox_Grid',
    __DIR__
);
