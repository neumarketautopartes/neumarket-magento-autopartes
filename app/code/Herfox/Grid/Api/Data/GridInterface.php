<?php
/**
 * Grid GridInterface.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */

namespace Herfox\Grid\Api\Data;

interface GridInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const TITLE = 'title';
    const CONTENT = 'webscraper_products';
    const PUBLISH_DATE = 'publish_date';
    const IS_ACTIVE = 'is_active';
    const UPDATE_TIME = 'update_time';
    const CREATED_AT = 'created_at';

   /**
    * Get EntityId.
    *
    * @return int
    */
    public function getEntityId();

   /**
    * Set EntityId.
    */
    public function setEntityId($entityId);

   /**
    * Get New Title.
    *
    * @return varchar
    */
    public function getNewTitle();

   /**
    * Set New Title.
    */
    public function setNewTitle($newTitle);

   /**
    * Get NewShortDescription.
    *
    * @return varchar
    */
    public function getNewShortDescription();

   /**
    * Set NewShortDescription.
    */
    public function setNewShortDescription($newShortDescription);
    /** -------------------------------------- */
    /**
    * Get NewLongDescription.
    *
    * @return varchar
    */
    public function getNewLongDescription();

   /**
    * Set NewLongDescription.
    */
    public function setNewLongDescription($newLongDescription);

    /**
    * Get NewgetFeatures.
    *
    * @return varchar
    */
    public function getNewFeatures();

   /**
    * Set NewgetFeatures.
    */
    public function setNewFeatures($newgetFeatures);
    /** -------------------------------------- */
    /** Nuevo Part Number Inicio */
     /**
    * Get NewPartNumber.
    *
    * @return varchar
    */
    public function getNewPartNumber();

   /**
    * Set getNewPartNumber.
    */
    public function setNewPartNumber($newPartNumber);
    /** Nuevo PArt Number Fin */
    /** Nuevo Fabricante Inicio */
     /**
    * Get NewFactory.
    *
    * @return varchar
    */
    public function getNewFactory();

   /**
    * Set setNewFactory.
    */
    public function setNewFactory($newFactory);
    /** Nuevo Fabricante Inicio */
    /** Nuevo Precio Inicio*/
     /**
    * Get NewPrice.
    *
    * @return varchar
    */
    public function getNewPrice();

   /**
    * Set setNewPrice.
    */
    public function setNewPrice($newPrice);
    /** Nuevo Precio Fin */
    /** Nuevo Stock Inicio */
     /**
    * Get NewStock.
    *
    * @return varchar
    */
    public function getNewStock();

   /**
    * Set setNewStock.
    */
    public function setNewStock($newStock);
    /** Nuevo Stock Fin */
    /** Nuevo Envio Inicio */
     /**
    * Get NewShipping.
    *
    * @return varchar
    */
    public function getNewShipping();

   /**
    * Set setNewShipping.
    */
    public function setNewShipping($newShipping);
    /** Nuevo Envio Fin */
   /**
    * Get Fecha registro.
    *
    * @return varchar
    */
    public function getPublishDate();

   /**
    * Set PublishDate.
    */
    public function setPublishDate($publishDate);

   /**
    * Get IsActive.
    *
    * @return varchar
    */
    public function getIsActive();

   /**
    * Set StartingPrice.
    */
    public function setIsActive($isActive);

   /**
    * Get UpdateTime.
    *
    * @return varchar
    */
    public function getUpdateTime();

   /**
    * Set UpdateTime.
    */
    public function setUpdateTime($updateTime);

   /**
    * Get CreatedAt.
    *
    * @return varchar
    */
    public function getCreatedAt();

   /**
    * Set CreatedAt.
    */
    public function setCreatedAt($createdAt);
}
