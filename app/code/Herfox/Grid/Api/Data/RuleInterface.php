<?php
/**
 * Rules RulesInterface.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */

namespace Herfox\Grid\Api\Data;

interface RuleInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const VALOR = 'valor';
    const REGLA = 'regla';
    const IS_ACTIVE = 'is_active';
    const UPDATE_TIME = 'update_time';
    const CREATED_AT = 'created_at';

   /**
    * Get EntityId.
    *
    * @return int
    */
    public function getEntityId();

   /**
    * Set EntityId.
    */
    public function setEntityId($entityId);

   /**
    * Get Valor.
    *
    * @return varchar
    */
    public function getValor();

   /**
    * Set Valor.
    */
    public function setValor($valor);

   /**
    * Get Regla.
    *
    * @return varchar
    */
    public function getRegla();

   /**
    * Set Regla.
    */
    public function setRegla($regla);
    /** -------------------------------------- */

   /**
    * Get IsActive.
    *
    * @return varchar
    */
    public function getIsActive();

   /**
    * Set StartingPrice.
    */
    public function setIsActive($isActive);

   /**
    * Get UpdateTime.
    *
    * @return varchar
    */
    public function getUpdateTime();

   /**
    * Set UpdateTime.
    */
    public function setUpdateTime($updateTime);

   /**
    * Get CreatedAt.
    *
    * @return varchar
    */
    public function getCreatedAt();

   /**
    * Set CreatedAt.
    */
    public function setCreatedAt($createdAt);
}
