<?php
/**
 * Herfox_ProductsGrid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model; 


class MasiveData  
{
    protected $_productCollectionFactory;
    protected $_attributeFactory;
    protected $_productAttributeRepository;
    protected $_yumValue;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Herfox\Grid\Model\ResourceModel\Grid\CollectionFactory $productData,
        \Magento\Eav\Model\Config $eavConfig
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory; 
        $this->_attributeFactory = $attributeFactory;   
        $this->_productAttributeRepository = $productAttributeRepository;
        $this->_eavConfig = $eavConfig;
        $this->_yumValue = $yumValue;
        $this->_productData = $productData;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getDataArray($part_number)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper-get-massivedata.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $product = $this->_productData->create();
        $product->addFieldToFilter('part_number',  $part_number);
        $productData = $product->getData();

        return $productData;
    }
}