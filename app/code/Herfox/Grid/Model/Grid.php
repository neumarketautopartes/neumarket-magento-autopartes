<?php

/**
 * Grid Grid Model.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Model;

use Herfox\Grid\Api\Data\GridInterface;

class Grid extends \Magento\Framework\Model\AbstractModel implements GridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'webscraper_products';

    /**
     * @var string
     */
    protected $_cacheTag = 'webscraper_products';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'webscraper_products';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Herfox\Grid\Model\ResourceModel\Grid');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Title.
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get shortDescription.
     *
     * @return varchar
     */
    public function getShortDescription()
    {
        return $this->getData(self::SHORT_DESCRIPTION);
    }

    /**
     * Set shortDescription.
     */
    public function setShortDescription($shortdescription)
    {
        return $this->setData(self::SHORT_DESCRIPTION, $shortdescription);
    }
    /** ------------------------------------------ */
    /** ------------------------------------------ */
     /**
     * Get RefCarId.
     *
     * @return varchar
     */
    public function getCaridItemNumber()
    {
        return $this->getData(self::CARID_ITEM_NUMBER);
    }

    /**
     * Set RefCarId.
     */
    public function setCaridItemNumber($caridItemNumber)
    {
        return $this->setData(self::CARID_ITEM_NUMBER, $caridItemNumber);
    }
    /** ------------------------------------------ */
/**
     * Get LongDescription.
     *
     * @return varchar
     */
    public function getLongDescription()
    {
        return $this->getData(self::LONG_DESCRIPTION);
    }

    /**
     * Set LongDescription.
     */
    public function setLongDescription($longDescription)
    {
        return $this->setData(self::LONG_DESCRIPTION, $longDescription);
    }

    /**
     * Get Features.
     *
     * @return varchar
     */
    public function getFeatures()
    {
        return $this->getData(self::FEATURES);
    }

    /**
     * Set Features.
     */
    public function setFeatures($features)
    {
        return $this->setData(self::FEATURES, $features);
    }
    

    /** ------------------------------------------ */
    /** Nuevo Nombre Inicio */
     /**
     * Get NewTitle.
     *
     * @return varchar
     */
    public function getNewTitle()
    {
        return $this->getData(self::NEW_TITLE);
    }

    /**
     * Set NewTitle.
     */
    public function setNewTitle($newTitle)
    {
        return $this->setData(self::NEW_TITLE, $newTitle);
    }
    /** Nuevo Nombre Fin */
    /** Nuevo Part Number Inicio */
    /**
     * Get NewPartNumber.
     *
     * @return varchar
     */
    public function getNewPartNumber()
    {
        return $this->getData(self::NEW_PART_NUMBER);
    }

    /**
     * Set NewPartNumber.
     */
    public function setNewPartNumber($newPartNumber)
    {
        return $this->setData(self::NEW_PART_NUMBER, $newPartNumber);
    }
    /** Nuevo Part Number Fin */
    /** Nuevo fabricante */
    /**
     * Get NewFactory.
     *
     * @return varchar
     */
    public function getNewFactory()
    {
        return $this->getData(self::NEW_FACTORY);
    }

    /**
     * Set NewFactory.
     */
    public function setNewFactory($newFactory)
    {
        return $this->setData(self::NEW_FACTORY, $newFactory);
    }
    /** Nuevo Fabricante */
    /** Nuevo Precio */
    /**
     * Get NewPrice.
     *
     * @return varchar
     */
    public function getNewPrice()
    {
        return $this->getData(self::NEW_PRICE);
    }

    /**
     * Set NewPrice.
     */
    public function setNewPrice($newPrice)
    {
        return $this->setData(self::NEW_PRICE, $newPrice);
    }
    /** Nuevo Precio */
    /** Nuevo Stock */
    /**
     * Get NewPrice.
     *
     * @return varchar
     */
    public function getNewStock()
    {
        return $this->getData(self::NEW_STOCK);
    }

    /**
     * Set NewStock.
     */
    public function setNewStock($newStock)
    {
        return $this->setData(self::NEW_STOCK, $newStock);
    }
    /** Nuevo Stock */
    /** Nuevo shipping */
    /**
     * Get NewShipping.
     *
     * @return varchar
     */
    public function getNewShipping()
    {
        return $this->getData(self::NEW_SHIPPING);
    }

    /**
     * Set NewShipping.
     */
    public function setNewShipping($newShipping)
    {
        return $this->setData(self::NEW_SHIPPING, $newShipping);
    }
    /** Nuevo Shipping */
    /** Nuevo NewShortDescription */
    /**
     * Get NewShortDescription.
     *
     * @return varchar
     */
    public function getNewShortDescription()
    {
        return $this->getData(self::NEW_SHORT_DESCRIPTION);
    }

    /**
     * Set NewShortDescription.
     */
    public function setNewShortDescription($newShortDescription)
    {
        return $this->setData(self::NEW_SHORT_DESCRIPTION, $newShortDescription);
    }
    /** Nuevo NewShortDescription */
    /** Nuevo NewLongDescription */
    /**
     * Get NewLongDescription.
     *
     * @return varchar
     */
    public function getNewLongDescription()
    {
        return $this->getData(self::NEW_LONG_DESCRIPTION);
    }

    /**
     * Set NewLongDescription.
     */
    public function setNewLongDescription($newLongDescription)
    {
        return $this->setData(self::NEW_LONG_DESCRIPTION, $newLongDescription);
    }
    /** Nuevo NewLongDescription */
    /** Nuevo NewFeatures */
    /**
     * Get NewFeatures.
     *
     * @return varchar
     */
    public function getNewFeatures()
    {
        return $this->getData(self::NEW_FEATURES);
    }

    /**
     * Set NewFeatures.
     */
    public function setNewFeatures($newFeatures)
    {
        return $this->setData(self::NEW_FEATURES, $newFeatures);
    }
    /** Nuevo NewFeatures */

    /**
     * Get PublishDate.
     *
     * @return varchar
     */
    public function getPublishDate()
    {
        return $this->getData(self::PUBLISH_DATE);
    }

    /**
     * Set PublishDate.
     */
    public function setPublishDate($publishDate)
    {
        return $this->setData(self::PUBLISH_DATE, $publishDate);
    }

    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set IsActive.
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
