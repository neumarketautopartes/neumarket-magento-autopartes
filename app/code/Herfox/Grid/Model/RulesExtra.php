<?php
/**
 * Herfox_ProductsGrid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model; 


class RulesExtra  
{
    protected $_categoryCollectionFactory;
    protected $_categoryHelper;
    protected $_attributeFactory;
    protected $_attributeGroupFactory;
    protected $_mapValue;
    protected $_yumValue;
    protected $_dropDonw;

    public function __construct(
     \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
     \Magento\Catalog\Helper\Category $categoryHelper,
     \Magebees\Finder\Model\ResourceModel\Mapvalue\CollectionFactory $mapValue,
     \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
     \Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory $attributeGroupFactory,
    \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
    \Magento\Eav\Model\Config $eavConfig,
    \Magebees\Finder\Model\ResourceModel\Dropdowns\CollectionFactory $dropDonw
    )
    {    
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->_attributeFactory = $attributeFactory;   
        $this->_attributeGroupFactory = $attributeGroupFactory;   
        $this->_mapValue = $mapValue;
        $this->_eavConfig = $eavConfig;
        $this->_yumValue = $yumValue;
        $this->_dropDonw = $dropDonw;
    }

    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getDataArray($ruleType,$dataType)
    {
      
    }

    public function getUniqueValues($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[$value['value']] = $value;
        }
        return $newArray;
    }

}
