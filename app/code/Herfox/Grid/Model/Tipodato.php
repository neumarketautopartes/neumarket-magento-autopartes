<?php
/**
 * Herfox_Grid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Tipodato implements OptionSourceInterface
{
    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $options = [
        '' => __('Seleccione'),
        '1' => __('Fabricante'),
        '3' => __('Categoria'),
        '4' => __('Envio'),
        '5' => __('iventario'),
        '6' => __('Precio'),
        '7' => __('Producto'),
        '8' => __('Marca'),
        '9' => __('Modelo'),
        '10' => __('Submodelo'),
        '11' => __('Conjunto de atributos'),
    ];
        return $options;
    }

    /**
     * Get Grid row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Grid row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
