<?php 
namespace Herfox\Grid\Model;

use \Magento\Framework\App\Bootstrap;
use \Herfox\Matriz\Controller\Adminhtml;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class CreateMagentoValue extends \Magento\Framework\Model\AbstractModel 
{


       private $categoryFactory;
    protected $_fileUploaderFactory;
    /** Finder Start */
    protected $_mapValue;
    protected $_yumValue;
    protected $_dropDonw;
    protected $_finderProduct;
    /** Finder End */

    public function __construct(
         CategoryFactory $categoryFactory,
        \Magebees\Finder\Controller\Adminhtml\Product\Save $finderProduct,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magebees\Finder\Model\ResourceModel\Mapvalue\CollectionFactory $mapValue,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Magebees\Finder\Model\ResourceModel\Dropdowns\CollectionFactory $dropDonw,
        \Magento\Framework\Filesystem $filesystem,
        \Herfox\Grid\Model\Rulesave $rulesave
    ) {
       
        $this->categoryFactory = $categoryFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        $this->_mapValue = $mapValue;
        $this->_yumValue = $yumValue;
        $this->_dropDonw = $dropDonw;
        $this->rulesave = $rulesave;
        $this->_finderProduct = $finderProduct;
        
    }

    
    public function getUniqueValues($data){
        
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[$value] = $value;
        }
        return $newArray;
    }
    public function clearArray($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[] = $value;
        }
        return $newArray;
    }
    public function createValue(){

    }

    
}