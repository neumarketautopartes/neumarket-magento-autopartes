<?php
/**
 * Herfox_ProductsGrid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model; 

class Rulesave  
{
    var $ruleFactory;
    var $collectionruleFactory;

    public function __construct(
        \Herfox\Grid\Model\ResourceModel\Rule\CollectionFactory $collectionruleFactory,
        \Herfox\Grid\Model\RuleFactory $ruleFactory
    ) {
        $this->_collectionruleFactory = $collectionruleFactory;
        $this->ruleFactory = $ruleFactory;
    }

    public function saveRule($ruletype,$data) 
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_in_rule.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        //$logger->info($data);
        switch($ruletype){
            case'manufacturer':
            $rules = $this->_collectionruleFactory->create()
            ->addFieldToFilter('valor', $data['manufacturer'])
            ->addFieldToFilter('rule_type', 2)
            ->addFieldToFilter('tipo_dato', 1);
            if(empty($rules->getData())){
                $rowData = $this->ruleFactory->create();
                $dataRule=array(
                    'valor' => $data['manufacturer'], 
                    'rule_type' => 2,
                    'tipo_dato' => 1,
                    'regla' => $data['new_manufacturer'],
                    'is_active' => 1,
                );
               //$logger->info(print_r($dataRule,true));
               $rowData->setData($dataRule);
               $rowData->save();
               return true;
            }
            break;
            case "brands":
            if(!empty($data['year_show'])){
                $logger->info(print_r($data,true)); 
                foreach($data['year_show'] as $key => $value){
                    
                    // Brand
                    if(!empty($data['brand'][$key])&&!empty($data['marca_old'][$key])){
                        $brand = $this->_collectionruleFactory->create()
                        ->addFieldToFilter('valor', $data['marca_old'][$key])
                        ->addFieldToFilter('rule_type', 3)
                        ->addFieldToFilter('tipo_dato', 8)
                        ->addFieldToFilter('is_active', 1);
                        if(empty($brand->getData())){
                            $rowData = $this->ruleFactory->create();
                            $dataRule=array(
                                'valor' => $data['marca_old'][$key], 
                                'rule_type' => 3,
                                'tipo_dato' => 8,
                                'regla' => $data['brand'][$key],
                                'is_active' => 1,
                            );
                            //$logger->info(print_r($dataRule,true));
                            $rowData->setData($dataRule);
                            $rowData->save();
                        }
                    }
                    // Model
                    if(!empty($data['modelo'][$key])){
                        $model = $this->_collectionruleFactory->create()
                        ->addFieldToFilter('valor', trim($data['modelo'][$key]))
                        ->addFieldToFilter('rule_type', 3)
                        ->addFieldToFilter('tipo_dato', 9)
                        ->addFieldToFilter('is_active', 1);
                        if(empty($model->getData())){
                            $rowData = $this->ruleFactory->create();
                            $dataRule=array(
                                'valor' => $data['modelo'][$key], 
                                'rule_type' => 3,
                                'tipo_dato' => 9,
                                'regla' => $data['modelo'][$key],
                                'is_active' => 1,
                            );
                            $logger->info(print_r($dataRule,true)); 
                            $rowData->setData($dataRule);
                            $rowData->save();
                        }
                    }
                    // Submodel
                    if(!empty($data['submodelo'][$key])&&!empty($data['submodelo_old'][$key])){
                        $submodel = $this->_collectionruleFactory->create()
                        ->addFieldToFilter('valor', $data['submodelo_old'][$key])
                        ->addFieldToFilter('rule_type', 3)
                        ->addFieldToFilter('tipo_dato', 10)
                        ->addFieldToFilter('is_active', 1);
                        if(empty($submodel->getData())){
                            $rowData = $this->ruleFactory->create();
                            $dataRule=array(
                                'valor' => $data['submodelo_old'][$key], 
                                'rule_type' => 3,
                                'tipo_dato' => 10,
                                'regla' => $data['submodelo'][$key],
                                'is_active' => 1,
                            );
                            //$logger->info(print_r($dataRule,true));
                            $rowData->setData($dataRule);
                            $rowData->save();
                        }
                    }
                }
                return true;
            }
            break;
            case "category":
            if(!empty($data['categories'])&&!empty($data['new_categories'])){
                $category = $this->_collectionruleFactory->create()
                ->addFieldToFilter('valor', trim($data['categories']))
                ->addFieldToFilter('rule_type', 1)
                ->addFieldToFilter('tipo_dato', 7)
                ->addFieldToFilter('is_active', 1);
                if(empty($category->getData())){
                    $rowData = $this->ruleFactory->create();
                    $dataRule=array(
                        'valor' => $data['categories'], 
                        'rule_type' => 1,
                        'tipo_dato' => 7,
                        'regla' => $data['new_categories'],
                        'is_active' => 1,
                    );
                    //$logger->info(print_r($dataRule,true));
                    $rowData->setData($dataRule);
                    $rowData->save();
                }
                return true;
            }
            break;
            case "attributes":
            if(!empty($data['new_attr_'])){
                foreach($data['new_attr_'] as $key => $value){
                    foreach($value as $keyAtt => $valueAtt){
                        if(!empty($valueAtt)){
                            $attribute = $this->_collectionruleFactory->create()
                            ->addFieldToFilter('valor', $data['attr_'][$key][$keyAtt])
                            ->addFieldToFilter('rule_type', 2)
                            ->addFieldToFilter('tipo_dato', 7)
                            ->addFieldToFilter('is_active', 1)
                            ->addFieldToFilter('attribute_set', $data['new_attributeset']);
                            if(empty($attribute->getData())){
                                $rowData = $this->ruleFactory->create();
                                $dataRule=array(
                                    'valor' => $data['attr_'][$key][$keyAtt], 
                                    'rule_type' => 2,
                                    'tipo_dato' => 7,
                                    'regla' => $valueAtt,
                                    'is_active' => 1,
                                    'attribute_set' => $data['new_attributeset'],
                                );
                                $logger->info(print_r($dataRule,true));
                                $rowData->setData($dataRule);
                                $rowData->save();
                            }
                       }
                    }
                }
                return true;
            }
            break;
            case "attributeset":
            if(!empty($data['attributeset'])&&!empty($data['new_attributeset'])){
                $attributeset = $this->_collectionruleFactory->create()
                ->addFieldToFilter('valor', trim($data['attributeset']))
                ->addFieldToFilter('rule_type', 2)
                ->addFieldToFilter('tipo_dato', 11)
                ->addFieldToFilter('is_active', 1);
               // $logger->info(print_r($attributeset->getSelect()->__toString(),true));
                if(empty($attributeset->getData())){
                    $rowData = $this->ruleFactory->create();
                    $dataRule=array(
                        'valor' => trim($data['attributeset']), 
                        'rule_type' => 2,
                        'tipo_dato' => 11,
                        'regla' => $data['new_attributeset'],
                        'is_active' => 1,
                    );
                    //$logger->info(print_r($dataRule,true));
                    $rowData->setData($dataRule);
                    $rowData->save();
                }
                return true;
            }
            break;
            default:
            break;


        }
      
        
    }

}
