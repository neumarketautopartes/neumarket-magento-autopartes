<?php

/**
 * Grid Rule Model.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Model;

use Herfox\Grid\Api\Data\RuleInterface;

class Rule extends \Magento\Framework\Model\AbstractModel implements RuleInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'rules';

    /**
     * @var string
     */
    protected $_cacheTag = 'rules';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'rules';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Herfox\Grid\Model\ResourceModel\Rule');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Valor.
     *
     * @return varchar
     */
    public function getValor()
    {
        return $this->getData(self::VALOR);
    }

    /**
     * Set Valor.
     */
    public function setValor($valor)
    {
        return $this->setData(self::VALOR, $valor);
    }

    /**
     * Get Regla.
     *
     * @return varchar
     */
    public function getRegla()
    {
        return $this->getData(self::REGLA);
    }

    /**
     * Set Regla.
     */
    public function setRegla($regla)
    {
        return $this->setData(self::REGLA, $regla);
    }
   

    /**
     * Set IsActive.
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
