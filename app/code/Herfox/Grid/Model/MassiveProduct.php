<?php 
namespace Herfox\Grid\Model;

use \Magento\Framework\App\Bootstrap;
use \Herfox\Matriz\Controller\Adminhtml;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class MassiveProduct extends \Magento\Framework\Model\AbstractModel 
{


       private $categoryFactory;
    protected $_fileUploaderFactory;
    /** Finder Start */
    protected $_mapValue;
    protected $_yumValue;
    protected $_dropDonw;
    protected $_finderProduct;
    /** Finder End */

    public function __construct(
         CategoryFactory $categoryFactory,
        \Magebees\Finder\Controller\Adminhtml\Product\Save $finderProduct,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magebees\Finder\Model\ResourceModel\Mapvalue\CollectionFactory $mapValue,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Magebees\Finder\Model\ResourceModel\Dropdowns\CollectionFactory $dropDonw,
        \Magento\Framework\Filesystem $filesystem,
        \Herfox\Grid\Model\Rulesave $rulesave
    ) {
       
        $this->categoryFactory = $categoryFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        $this->_mapValue = $mapValue;
        $this->_yumValue = $yumValue;
        $this->_dropDonw = $dropDonw;
        $this->rulesave = $rulesave;
        $this->_finderProduct = $finderProduct;
        
    }
    
    public function getUniqueValues($data){
        
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[$value] = $value;
        }
        return $newArray;
    }
    public function clearArray($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[] = $value;
        }
        return $newArray;
    }
    public function createProduct($data)
	{
          
        include('../app/bootstrap.php');
        $bootstrap = Bootstrap::create(BP, $_SERVER);
        $objectManager = $bootstrap->getObjectManager();
        $url = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
        $mediaurl= $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $state = $objectManager->get('\Magento\Framework\App\State');
        $state->setAreaCode('frontend');
        /// Get Website ID
        $websiteId = $storeManager->getWebsite()->getWebsiteId();
        //echo 'websiteId: '.$websiteId." ";

        /// Get Store ID
        $store = $storeManager->getStore();
        $storeId = $store->getStoreId();
        //echo 'storeId: '.$storeId." ";

        /// Get Root Category ID
        $rootNodeId = $store->getRootCategoryId();
        //echo 'rootNodeId: '.$rootNodeId." ";
        /// Get Root Category
        $rootCat = $objectManager->get('Magento\Catalog\Model\Category');
        $cat_info = $rootCat->load($rootNodeId);

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_create_product.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer); 
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('webscraper_products'); 

        $categories = ','.$data['new_categories'].',62';
        $revCat = explode(',',$categories);
        $categories = array_filter($revCat);
       // $logger->info(print_r($data,true));
    
         /** ---------------------------------- */
         
            $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
            $path = '/var/www/html/magento2/pub/media/';
            $logger->info('Información recibida --- Start');
            $logger->info(print_r($data,true));
            $logger->info('Información recibida --- End');
            if(empty($data['new_attributeset'])){
                $data['new_attributeset'] = 4;
            }

            /** ------ Creación de Producto Simple Inicio */
            $logger->info('Creando Producto simple  '.$data['new_title'].' ');
            $img = $data['image_url'];
                if(!empty($img)){
                    $logger->info('imagen para descarga: '.$img);
                    $imageName = explode('/',$img);
                    if(!empty($imageName)){
                        $last = count($imageName)-1;
                        if(!empty($last)){
                            $siteurl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                            $logger->info('url sitio web: '.$siteurl);
                            $file_headers = get_headers($siteurl.'pub/media/'.$imageName[$last]);
                            //$logger->info('headers of file: '.print_r($file_headers,true));
                            if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                copy($img, $path.$imageName[$last]);
                            }
                            $mediaImagenUrl = $siteurl.'pub/media/'.$imageName[$last];
                            $logger->info('Imagen final: '.$mediaImagenUrl);
                        }
                    }
                }
                
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($data['part_number']); 
            $product->setName($data['new_title'].' '.$data['part_number']); 
            $product->setAttributeSetId($data['new_attributeset']); 
            $product->setShortDescription($data['new_short_description']); 
            $product->setDescription($data['new_long_description']); 
            $product->setStatus(1); 
            $product->setWeight(10); 
            $product->setCategoryIds($categories);
            if(!empty($data['new_attr_'])){
                foreach($data['new_attr_'] as $key => $value){
                    foreach($value as $keyAtt => $valueAtt){
                        if(!empty($valueAtt)){
                            $attributo = explode('-',$valueAtt);
                            $eavModel = $objectManager->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');
                            $attr = $eavModel->load($attributo[0]);
                            $attributeCode=$eavModel->getAttributeCode();
                            $parts = explode('_',$attributeCode);
                            $codeAtt = 'set';
                            foreach($parts as $part){
                                $codeAtt .= ucfirst($part);
                            }
                            $product->$codeAtt($attributo[1]);
                       }
                    }
                }
            }


            if(!empty($data['new_manufacturer'])){
                $product->setManufacturer($data['new_manufacturer']); 
            }
            if(!empty($data['urk'])){
                $product->setUrlCarid($data['urk']); 
            }
            $product->setVisibility(4); 
            $product->setWebsiteIds(array(1));
            $product->setTaxClassId(0); 
            $product->setTypeId('simple'); 
            $mediaGalleryProcessor = $objectManager->get('Magento\Catalog\Model\Product\Gallery\Processor');
            $mediaGalleryProcessor->addImage($product,$dir->getPath('media').'/'.$imageName[$last],array('image','thumbnail','small_image'), false, false);
            $product->setPrice($data['new_price']); 
            $product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => (($data['new_stock']>0)?1:0),
                    'qty' => $data['new_stock'] 
                )
            );
            if($product->save()){
                $childs['id'][] =  $product->getId();
                $childs['sku'][] = $data['part_number'];
                /** ----- Creando Marcas Inicio ------- */
                if(isset($data['brand'])){
                    foreach($data['brand'] as $key => $value){
                        if(!empty($value)&&!empty($data['year_show'][$key])&&!empty($data['modelo'][$key])&&!empty($data['submodelo'][$key])){
                           // $verifyrelation = $this->verifyRelationV2($data['year'][$key],$value,$data['modelo'][$key],$data['submodelo'][$key]);
                          //  if(!empty($verifyrelation)){
                                
                                    $arrayId = $childs['id'];
                                    $cadenaId = '';
                                    $skuFinder = array();
                                    foreach($arrayId as $keyId => $valueId){
                                        $productReal = $objectManager->get('Magento\Catalog\Model\Product')->load($valueId);
                                        if(!empty($productReal->getName())){
                                            $cadenaId .= $valueId.'&'; 
                                        }
                                    }
                                    
                                    
                                    $dataSend['label_4'] = $data['year_show'][$key];
                                    $dataSend['label_5'] = $value;
                                    $dataSend['label_6'] = $data['modelo'][$key];
                                    $dataSend['label_7'] = $data['submodelo'][$key];
                                    $dataSend['links']= array(
                                        'finder' => $cadenaId
                                    );
                                    
                                    $dataSend['finder_id'] = 2;
                                    $dataSend['webscraper'] = 'yes';
                                    $logger->info('data to finder: '.print_r($dataSend,true));
                                    $finderSave = $this->_finderProduct->execute($dataSend);
                           // }
                        }
                    }
                
                }
            /** ------ Creación de Producto Simple Fin */
                $sqlUpdateStatus = "UPDATE " . $tableName . " SET is_active = '3' WHERE entity_id ='".$data['entity_id']."' ";
                $connection->query($sqlUpdateStatus);
        }
        

    }
}