<?php
/**
 * Herfox_Grid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model;

use Magento\Framework\Data\OptionSourceInterface;

class AttributeSet implements OptionSourceInterface
{
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory
    )
    {    
        $this->collectionFactory = $collectionFactory;
    }
    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $attributeSetCollection = $this->collectionFactory->create();
         $attributeSets = $attributeSetCollection->getItems();
         $options  = array();
        $options[''] = 'Seleccione';
         $attributeNoAcepted = array(1,2,3,5,6,7,8,9,10,11,12,13,14,15,16);
         foreach ($attributeSets as $attributeSet) {
            if(!in_array($attributeSet->getAttributeSetId(),$attributeNoAcepted)){
                $options[$attributeSet->getAttributeSetId()] = $attributeSet->getAttributeSetName();
            }
        }
        return $options;
    }

    /**
     * Get Grid row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Grid row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
