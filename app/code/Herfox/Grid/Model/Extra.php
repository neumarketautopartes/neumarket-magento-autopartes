<?php
/**
 * Herfox_ProductsGrid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model; 


class Extra  
{
    protected $_productCollectionFactory;
    protected $_attributeFactory;
    protected $_productAttributeRepository;
    protected $_yumValue;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Magento\Eav\Model\Config $eavConfig
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory; 
        $this->_attributeFactory = $attributeFactory;   
        $this->_productAttributeRepository = $productAttributeRepository;
        $this->_eavConfig = $eavConfig;
        $this->_yumValue = $yumValue;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getDataArray($part_number)
    {
        
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper-extra.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $productsTable = $resource->getTableName('webscraper_products');

        $sql = "SELECT * FROM ".$productsTable." WHERE part_number='".$part_number."'";
        $resultSql = $connection->fetchAll($sql); 
        /** ------------------------------- */
        $brans = array();
        $sqlBrandsParent = "SELECT * FROM webscraper_brands WHERE part_number='".$part_number."'";
        $resultBrandsParent = $connection->fetchAll($sqlBrandsParent);
        foreach($resultBrandsParent as $keyRel => $valRel){
            // $logger->info(print_r($valRel,true)); 
             $brans[] = array(
                 'cat' => 'Brake Parts',
                 'marca' => $valRel['marca'],
                 'modelo' => $valRel['modelo'],
                 'year' => $valRel['year'], 
             );
         }
         // CONJUNTO DE ATRIBUTOS INICIO
         $attributeSetCollection = $this->collectionFactory->create();
         $attributeSets = $attributeSetCollection->getItems();
         $optionsSet = '<option>Seleccione</option>';
         $attributeSetTable = '<table align=\'center\' class=\'table-child\'>';
         $attributeSetTable .= '<thead class=\'header-hf\'>';
         $attributeSetTable .= '<th>';
         $attributeSetTable .= 'Conjunto de Atributos';
         $attributeSetTable .= '</th>';
         $attributeSetTable .= '</thead>';
         $attributeNoAcepted = array(1,2,3,5,6,7,8,9,10,11,12,13,14,15,16);
         //$selected = $resultBrandsParent[0]['atributo'];
         foreach ($attributeSets as $attributeSet) {
             if(!in_array($attributeSet->getAttributeSetId(),$attributeNoAcepted)){
                 $optionsSet .= '<option value="'.$attributeSet->getAttributeSetId().'" >'.$attributeSet->getAttributeSetName().'</option>';
             }
         // echo $attributeSet->getAttributeSetName() . chr(10) . chr(13);
             //$logger->info('attribute set: '.$attributeSet->getAttributeSetName().' - '.$attributeSet->getAttributeSetId());
         // $logger->info('attribute set: '.print_r($attributeSet->getData(),true));
         }
         $attributeSetTable .= '<tr>';
         $attributeSetTable .= '<td>';
         $attributeSetTable .= '<input class=\'traslate-text-hfx\' name=\'attributeset\' id=\'wpgrid_attributeset\'  type=\'hidden\' value=\'Brake Pads\' />'; //  $selected = $resultBrandsParent[0]['atributo']; REEMPLAZA DESPUES DE AJUSTAR
         $attributeSetTable .= '<select id=\'wpgrid_new_attributeset\'  class=\'required-entry admin__control-select select-attribute_set\'    name=\'new_attributeset\' >'.$optionsSet.'</select>';
         $attributeSetTable .= '</td>';
         $attributeSetTable .= '</tr>';
         $attributeSetTable .= '<tr>';
         $attributeSetTable .= '<td>';
         $attributeSetTable .= '<strong>NOTA:</strong> debe tener en cuenta que cada tipo de producto tiene un conjunto de attributos asignado,<br> los atributos de la siguiente tabla estan ligados a este desplegable, si cambia lo cambia el listado de atributos tambien cambiará.';
         $attributeSetTable .= '</td>';
         $attributeSetTable .= '</tr>';
         $attributeSetTable .= '</table>';
         // CONJUNTO DE ATRIBUTOS FIN
        /** ------------------------------- */
        if(!empty($resultSql)){

            
            
             //$collection = $this->_productCollectionFactory->create();
            // $collection->addAttributeToSelect('*');
            $attributeInfo = $this->_attributeFactory->getCollection()
                 ->addFieldToFilter(\Magento\Eav\Model\Entity\Attribute\Set::KEY_ENTITY_TYPE_ID, 4);
                // ->addFieldToFilter('attribute_code',array('in' => array('material_de_freno')));
                 $optionsAtt = '<option value="" >Seleccione</option>';
                 foreach($attributeInfo->getData() as $keyAtt => $valAtt){
                    // $logger->info(print_r($valAtt,true));
                    $attributeCode = $valAtt['attribute_code'];
                    $attribute = $this->_eavConfig->getAttribute('catalog_product', $attributeCode);
                    $options = $attribute->getSource()->getAllOptions();

                     if(!empty($valAtt['frontend_label'])){
                        $optionsAtt .= '<optgroup label="'.$valAtt['frontend_label'].'">';
                        foreach ($options as $option) {
                            if ($option['value'] > 0) {
                                $optionsAtt .= '<option  value="'.$valAtt['attribute_id'].'-'.$option['value'].'" >'.$option['label'].'</option>';
                            }
                        }
                        $optionsAtt .= '</optgroup>';
                    }
                 }


            $attributeproduct = '<table align=\'center\' class=\'table-child\'>';
            $attributeproduct .= '<thead class=\'header-hf\'>';
            $attributeproduct .= '<th>';
            $attributeproduct .= 'Id';
            $attributeproduct .= '</th>';
            $attributeproduct .= '<th>';
            $attributeproduct .= 'Atributo';
            $attributeproduct .= '</th>';
            $attributeproduct .= '<th>';
            $attributeproduct .= 'Opción original';
            $attributeproduct .= '</th>';
            $attributeproduct .= '<th>';
            $attributeproduct .= 'Opción asignada';
            $attributeproduct .= '</th>';
            $attributeproduct .= '</thead>';

            $relproduct = '<table align=\'center\' class=\'table-child\'>';
            $relproduct .= '<thead class=\'header-hf\'>';
            $relproduct .= '<th>';
            $relproduct .= 'Id';
            $relproduct .= '</th>';
            $relproduct .= '<th>';
            $relproduct .= 'Imagen';
            $relproduct .= '</th>';
            $relproduct .= '<th>';
            $relproduct .= 'Part Number';
            $relproduct .= '</th>';
            $relproduct .= '<th>';
            $relproduct .= 'Nombre';
            $relproduct .= '</th>';
            $relproduct .= '<th>';
            $relproduct .= 'Inventario';
            $relproduct .= '</th>';
            $relproduct .= '<th>';
            $relproduct .= 'Precio';
            $relproduct .= '</th>';
            $relproduct .= '</thead>';
            
            foreach($resultSql as $key => $value){
                $sqlAttributes = 'SELECT * FROM webscraper_attributes_rel AS rel JOIN webscraper_attributes AS attr ON rel.attribute_id=attr.id  WHERE rel.part_number="'.$value['part_number'].'" ';
                $resultAttributes = $connection->fetchAll($sqlAttributes); 
                $auto = $key+1;
                $relproduct .= '<tr>';
                $relproduct .= '<td>';
                $relproduct .= $key+1; 
                $relproduct .= '</td>';
                $relproduct .= '<td>';
                $relproduct .= '<img src="'.$value['image_url'].'" style="width:120px;" >'; 
                $relproduct .= '</td>';
                $relproduct .= '<td>';
                $relproduct .= $value['part_number'].'<input  name=\'part_number_child['.$auto.']\'  type=\'hidden\' value=\''.$value['part_number'].'\' />'; 
                $relproduct .= '</td>';
                $relproduct .= '<td>';
                $relproduct .= $value['title'].'<br><input class=\'traslate-text-hfx\' id=\'wpgrid_child_'.$auto.'\'  type=\'hidden\' value=\''.$value['title'].'\' /><input id=\'wpgrid_new_child_'.$auto.'\' type=\'text\' class=\' input-text input-text-hf admin__control-text\' name=\'new_name_child['.$auto.']\' value=\''.$value['title'].'\' />';
                $relproduct .= '</td>';
                $relproduct .= '<td>'; 
                $relproduct .= $value['stock'].'<br><input type=\'text\' class=\' input-text admin__control-text\' name=\'new_stock_child['.$auto.']\' value=\''.((empty($value['stock']))?10:intval(preg_replace('/[^0-9]+/', '', $value['stock']), 10)).'\' />';
                $relproduct .= '</td>';
                $relproduct .= '<td>';
                $relproduct .= $value['price'].'<br><input type=\'text\' class=\' input-text admin__control-text\' name=\'new_price_child['.$auto.']\' value=\''.$value['price'].'\' />';
                $relproduct .= '</td>';
                
                
                foreach($resultAttributes as $attrId => $atrr){
                    $sqlOptios = "SELECT * FROM webscraper_attributes_options WHERE id='".$atrr['option_id']."' ";
                    $resultoptions = $connection->fetchAll($sqlOptios);
                    $attributeproduct .= '<tr>';
                    $attributeproduct .= '<td>';
                    $attributeproduct .= $value['part_number'];
                    $attributeproduct .= '</td>'; 
                    $attributeproduct .= '<td>';
                    $attributeproduct .= $atrr['nombre'];
                    $attributeproduct .= '</td>'; 
                    /*$attributeproduct .= '<td>';
                    $attributeproduct .= $atrr['nombre'];
                    $attributeproduct .= '</td>'; */
                    $attributeproduct .= '<td>';
                    $attributeproduct .= $resultoptions[0]['nombre'].'<input class=\'traslate-text-hfx\' name=\'attr_['.$value['part_number'].']['.$atrr['option_id'].']\' type=\'hidden\' id=\'wpgrid_attr_'.$atrr['option_id'].'\'  value=\''.$resultoptions[0]['nombre'].'\' />';
                    $attributeproduct .= '</td>';
                    $attributeproduct .= '<td>';
                    $attributeproduct .= '<select  class=\' input-select required-entry admin__control-select attributos_producto_hf\' id=\'wpgrid_new_attr_'.$atrr['option_id'].'\' name=\'new_attr_['.$value['part_number'].']['.$atrr['option_id'].']\'  >'.$optionsAtt.'</select>';
                    $attributeproduct .= '</td>';
                    $attributeproduct .= '</tr>';
                   // $logger->info(print_r($atrr['nombre'],true));
                }
                $relproduct .= '</tr>';
                /** ---------------------- */
               /* $sqlBrands = "SELECT * FROM webscraper_brands WHERE part_number='".$value['part_number']."'";
                $resultBrands = $connection->fetchAll($sqlBrands); 
                foreach($resultBrands as $keyRel => $valRel){
                    $brans[] = array(
                        'cat' => 'Brake Parts',
                        'marca' => $valRel['marca'],
                        'modelo' => $valRel['modelo'],
                        'year' => $valRel['year'],
                    );
                }*/
                /** ---------------------- */
            }
            $relproduct .='</table>';
            $attributeproduct .='</table>';
        }else{
            $relproduct = '';
            $attributeproduct = '';
        }
            /** -------------------------------- */
            if(!empty($resultBrandsParent)){
            $tableMarcas = '<table align=\'center\' class=\'table-child\'>';
            $tableMarcas .= '<thead class=\'header-hf\' >';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Año';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Marca Car Id';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Marca Asignada';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Modelo Car Id';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Modelo Asignado';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Submodelo Car Id';
            $tableMarcas .= '</th>';
            $tableMarcas .= '<th>';
            $tableMarcas .= 'Submodelo Asignado ';
            $tableMarcas .= '</th>';
            $tableMarcas .= '</thead>';
            foreach($brans as $keyData => $valueData){
                
                $years = $this->_yumValue->create();
                $years->addFieldToFilter('dropdown_id',  4);
                $years->addFieldToFilter('value', $valueData['year']);
                $yearData = $years->getData();

               // $logger->info(print_r($years->getSelect()->__toString(),true));
                
                $selectMarca = '<option value="" >Seleccione</option>';
                $selectModel = '<option value="" >Seleccione</option>';
                $selectSubmodel = '<option value="" >Seleccione</option>';
                //if(!empty($yearData)){
                $brand = $this->_yumValue->create();
                $brand->addFieldToFilter('dropdown_id',  5);
                $brandData = $brand->getData();
                
                if(!empty($brandData)){
                    foreach($brandData as $brand => $value){
                        $selectMarca .= '<option value="'.$value['value'].'" >'.$value['value'].'</option>';
                    }
                }
            
                
                
                $auto = $keyData+1;
                $tableMarcas .= '<tr>';
                $tableMarcas .= '<td>';
                $tableMarcas .= '<input type=\'hidden\'  readonly  name=\'year['.$auto.']\' value=\''.((!empty($yearData))?$yearData[0]['ymm_value_id']:'').'\' />';
                $tableMarcas .= '<input type=\'text\' style=\'width:7rem; background-color:#f5f5f5; text-align:center\' readonly class=\'required-entry input-text admin__control-text\' name=\'year_show['.$auto.']\' value=\''.$valueData['year'].'\' />';
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= $valueData['marca'];
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= '<input class=\'traslate-text-hfx\' name=\'marca_old['.$auto.']\' id=\'wpgrid_marca_'.$auto.'\'  type=\'hidden\' value=\''.$valueData['marca'].'\' /><input id=\'old_wpgrid_new_marca_'.$auto.'\' type=\'hidden\' class=\'required-entry input-text admin__control-text\' name=\'brand['.$auto.']\' value=\''.$valueData['marca'].'\' />';
                $tableMarcas .= '<select id=\'wpgrid_new_marca_'.$auto.'\'  class=\'required-entry admin__control-select select-brand\' data-target=\'wpgrid_new_modelo_'.$auto.'\' data-dropdown=\'5\' data-dropdownnext=\'6\'  name=\'brand['.$auto.']\' >'.$selectMarca.'</select>';
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= $valueData['modelo'];
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= '<input class=\'traslate-text-hfx\' name=\'modelo_old['.$auto.']\' id=\'wpgrid_modelo_'.$auto.'\'   type=\'hidden\' value=\''.$valueData['modelo'].'\' /><input id=\'old_wpgrid_new_modelo_'.$auto.'\' type=\'hidden\' class=\'required-entry input-text admin__control-text\' name=\'modelo['.$auto.']\' value=\''.$valueData['modelo'].'\' />';
                $tableMarcas .= '<select data-target=\'wpgrid_new_submodelo_'.$auto.'\' data-dropdown=\'6\' data-dropdownnext=\'7\' id=\'wpgrid_new_modelo_'.$auto.'\' class=\'required-entry admin__control-select select-brand\' name=\'modelo['.$auto.']\'  />'.$selectModel.'</select>';
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= ((isset($valueData['submodelo']))?$valueData['submodelo']:'');
                $tableMarcas .= '</td>';
                $tableMarcas .= '<td>';
                $tableMarcas .= '<input class=\'traslate-text-hfx\' name=\'submodelo_old['.$auto.']\' id=\'wpgrid_submodelo_'.$auto.'\'  type=\'hidden\' value=\''.((isset($valueData['submodelo']))?$valueData['submodelo']:'').'\' /><input id=\'old_wpgrid_new_submodelo_'.$auto.'\' type=\'hidden\' class=\'input-text admin__control-text\' name=\'submodelo['.$auto.']\' value=\''.((isset($valueData['submodelo']))?$valueData['submodelo']:'').'\' />';
                $tableMarcas .= '<select id=\'wpgrid_new_submodelo_'.$auto.'\'  class=\'admin__control-select\' name=\'submodelo['.$auto.']\'  >'.$selectSubmodel.'</select>';
                $tableMarcas .= '</td>';
                $tableMarcas .= '</tr>';
            }
            $tableMarcas .= '</table>';
            /** -------------------------------- */
        }else{
            $tableMarcas = '';
        }
        $relproduct = array();
        $options = array(
            'childs' => $relproduct, 
            'brands' => $tableMarcas,
            'conjunto_atributos' => $attributeSetTable,
            'atributos' => $attributeproduct,
            'category' => 'Brakes Full'.$part_number,
        );
      //  $logger->info(print_r($attributeproduct,true));
    return $options;
    }

    public function getUniqueValues($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[$value['value']] = $value;
        }
        return $newArray;
    }

}
