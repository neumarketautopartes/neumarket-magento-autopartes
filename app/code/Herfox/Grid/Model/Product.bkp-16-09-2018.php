<?php 
namespace Herfox\Grid\Model;

use \Magento\Framework\App\Bootstrap;
use \Herfox\Matriz\Controller\Adminhtml;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Product extends \Magento\Framework\Model\AbstractModel 
{

    private $_marca;
    private $_modelo;
    private $_submodelo;
    private $_matriz;
    private $_marcaFactory;
    private $_modeloFactory;
    private $_submodeloFactory;
    private $_matrizFactory;
    private $categoryFactory;
    protected $_fileUploaderFactory;

    public function __construct(
         CategoryFactory $categoryFactory,
        \Herfox\Matriz\Controller\Adminhtml\Marca\Save $marca,
        \Herfox\Matriz\Controller\Adminhtml\Modelo\Save $modelo,
        \Herfox\Matriz\Controller\Adminhtml\Submodelo\Save $submodelo,
        \Herfox\Matriz\Controller\Adminhtml\Contacts\Save $matriz,
        \Herfox\Matriz\Model\ResourceModel\Marca\CollectionFactory $marcaFactory,
        \Herfox\Matriz\Model\ResourceModel\Modelo\CollectionFactory $modeloFactory,
        \Herfox\Matriz\Model\ResourceModel\Submodelo\CollectionFactory $submodeloFactory,
        \Herfox\Matriz\Model\ResourceModel\Contact\Grid\CollectionFactory $matrizFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->_marca = $marca;
        $this->_modelo = $modelo;
        $this->_submodelo = $submodelo;
        $this->_matriz = $matriz;
        $this->_marcaFactory = $marcaFactory;
        $this->_modeloFactory = $modeloFactory;
        $this->_submodeloFactory = $submodeloFactory;
        $this->_matrizFactory = $matrizFactory;
        $this->categoryFactory = $categoryFactory;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        
    }

    public function createProduct($data)
	{
        
        include('../app/bootstrap.php');
        $bootstrap = Bootstrap::create(BP, $_SERVER);
        $objectManager = $bootstrap->getObjectManager();
        $url = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
        $mediaurl= $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $state = $objectManager->get('\Magento\Framework\App\State');
        $state->setAreaCode('frontend');
        /// Get Website ID
        $websiteId = $storeManager->getWebsite()->getWebsiteId();
        //echo 'websiteId: '.$websiteId." ";

        /// Get Store ID
        $store = $storeManager->getStore();
        $storeId = $store->getStoreId();
        //echo 'storeId: '.$storeId." ";

        /// Get Root Category ID
        $rootNodeId = $store->getRootCategoryId();
        //echo 'rootNodeId: '.$rootNodeId." ";
        /// Get Root Category
        $rootCat = $objectManager->get('Magento\Catalog\Model\Category');
        $cat_info = $rootCat->load($rootNodeId);

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_create_product.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer); 
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('webscraper_products'); 



        /** ---- Crear categorias Inicio */
        $categorys=$data['category']; // Category Names
        foreach($categorys as $cat)
        {
            $categoryId = '';
            $name=ucfirst($cat);
            $url=strtolower($cat);
            $cleanurl = trim(preg_replace('/ +/', '', preg_replace('/[^A-Za-z0-9 ]/', '', urldecode(html_entity_decode(strip_tags($url))))));
            $categoryFactory = $this->categoryFactory->create();
            $category = $categoryFactory->loadByAttribute('url_key', $cleanurl);
            if(!empty($category)){
                $categories = $category->getId();
            }
            
            try {
                
            if(empty($categories)){
                $categoryFactory=$objectManager->get('\Magento\Catalog\Model\CategoryFactory');
                /// Add a new sub category under root category
                $categoryTmp = $categoryFactory->create();
                $categoryTmp->setName($name);
                $categoryTmp->setIsActive(true);
                $categoryTmp->setUrlKey($cleanurl);
                $categoryTmp->setData('description', 'description');
                $categoryTmp->setParentId($rootCat->getId());
                $mediaAttribute = array ('image', 'small_image', 'thumbnail');
                $categoryTmp->setImage('/m2.png', $mediaAttribute, true, false);// Path pub/meida/catalog/category/m2.png
                $categoryTmp->setStoreId($storeId);
                $categoryTmp->setPath($rootCat->getPath());
                if($categoryTmp->save()){
                    $categoryFactory = $this->categoryFactory->create();
                    $category = $categoryFactory->loadByAttribute('url_key', $cleanurl);
                    $categories = $category->getId();
                }
            }
            } catch (\Exception $e) {
                //$this->messageManager->addError(__($e->getMessage()));
            }
           
            
        }
        /** ---- Crear categorias Fin */
        
         /** ---------------------------------- */
         
         

        if($data['is_active']=='2'){
            $path = '/var/www/html/magento2/pub/media/';
            $logger->info('Información recibida --- Start');
            $logger->info(print_r($data,true));
            $logger->info('Información recibida --- End');
        /** Crear hijos Inicio */
        if(!empty($data['part_number_child'])){
            $childs = array();
            $logger->info('Producto configurable  con '.count($data['part_number_child']).' productos relacionados ');
            foreach($data['part_number_child'] as $key =>  $value){
                
                $sql = "SELECT * FROM webscraper_products WHERE part_number='".$value."' ";
                $resulSql = $connection->fetchAll($sql); 

               // $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
                
                
                $logger->info($value);
                if(!empty($resulSql)){

                $img = $resulSql[0]['image_url'];
                if(!empty($img)){
                    $logger->info('imagen para descarga: '.$img);
                    $imageName = explode('/',$img);
                    if(!empty($imageName)){
                        $last = count($imageName)-1;
                        if(!empty($last)){
                            $siteurl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                            $logger->info('url sitio web: '.$siteurl);
                            copy($img, $path.$imageName[$last]);
                            $mediaImagenUrl = $siteurl.'pub/media/'.$imageName[$last];
                            $logger->info('Imagen final: '.$mediaImagenUrl);
                        }
                    }
                }
                $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
                
                    $logger->info('Creando  '.$data['new_name_child'][$key].' ');
                    $product = $objectManager->create('\Magento\Catalog\Model\Product');
                    $product->setSku($resulSql[0]['part_number']); 
                    $product->setName($data['new_name_child'][$key]); 
                    $product->setAttributeSetId(4); 
                    $product->setShortDescription($data['new_short_description'].'. '.$data['new_name_child'][$key]); 
                    $product->setDescription($data['new_short_description'].". ".$data['new_name_child'][$key]); 
                    $product->setStatus(1); 
                    $product->setWeight(10); 
                    $product->setCategoryIds($categories);
                    $product->setVisibility(4); 
                    $product->setWebsiteIds(array(1));
                    $product->setTaxClassId(0); 
                    $product->setTypeId('simple'); 
                    $product->setPrice($data['new_price_child'][$key]); 
                    $mediaGalleryProcessor = $objectManager->get('Magento\Catalog\Model\Product\Gallery\Processor');
                    $mediaGalleryProcessor->addImage($product,$dir->getPath('media').'/'.$imageName[$last],array('image','thumbnail','small_image'), false, false);
                    $logger->info('ruta imagen: '.$dir->getPath('media').'/'.$imageName[$last]);
                    $product->setStockData(
                        array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'is_in_stock' => (($data['new_stock']>0)?1:0),
                            'qty' => $data['new_stock'] 
                        )
                    );
                    
                    if($product->save()){
                        $childs[] = $product->getId();
                    }
                }
            }
            /** Crear hijos Fin */
        /**  -------- Creacion de producto configurable Inicio ----------- */


            $logger->info('Creando  '.$data['new_title'].' ');
            $img = $data['image_url'];
                if(!empty($img)){
                    $logger->info('imagen para descarga: '.$img);
                    $imageName = explode('/',$img);
                    if(!empty($imageName)){
                        $last = count($imageName)-1;
                        if(!empty($last)){
                            $siteurl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                            $logger->info('url sitio web: '.$siteurl);
                            copy($img, $path.$imageName[$last]);
                            $mediaImagenUrl = $siteurl.'pub/media/'.$imageName[$last];
                            $logger->info('Imagen final: '.$mediaImagenUrl);
                        }
                    }
                }
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($data['part_number']); 
            $product->setName($data['new_title']); 
            $product->setAttributeSetId(4); 
            $product->setShortDescription($data['new_short_description']); 
            $product->setDescription($data['new_long_description']); 
            $product->setStatus(1); 
            $product->setWeight(10); 
            $product->setCategoryIds($categories);
            $product->setVisibility(4); 
            $product->setWebsiteIds(array(1));
            $product->setTaxClassId(0); 
            $product->setTypeId('configurable'); 
            $mediaGalleryProcessor = $objectManager->get('Magento\Catalog\Model\Product\Gallery\Processor');
            $mediaGalleryProcessor->addImage($product,$dir->getPath('media').'/'.$imageName[$last],array('image','thumbnail','small_image'), false, false);
            $product->setPrice(0); 
            $product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => (($data['new_stock']>0)?1:0),
                    'qty' => $data['new_stock'] 
                )
            );
            if($product->save()){
                $productId = $product->getId();
                $logger->info("Produto configurable id:  ".$productId);
                
                /*$product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId); // Load Configurable Product
                $attributeModel = $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute');
                $position = 0;
                $attributes = array(312); // Super Attribute Ids Used To Create Configurable Product
                $associatedProductIds = $childs; //Product Ids Of Associated Products
                $logger->info("Productos relacionados configurable  ".print_r($childs,true));
                foreach ($attributes as $attributeId) {
                    $dataAttr = array('attribute_id' => $attributeId, 'product_id' => $productId, 'position' => $position);
                    $logger->info("Asociación de atributos:  ".print_r($dataAttr,true));
                    $position++;
                    $attributeModel->setData($dataAttr)->save();
                }
                $product->setTypeId("configurable"); // Setting Product Type As Configurable
                $product->setAffectConfigurableProductAttributes(4);
                $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable')->setUsedProductAttributeIds($attributes, $product);
                $product->setNewVariationsAttributeSetId(4); // Setting Attribute Set Id
                $product->setAssociatedProductIds($associatedProductIds);// Setting Associated Products
                $product->setCanSaveConfigurableAttributes(true);*/
               
                if($product->save()){
                    /** ----- Creando Marcas Inicio ------- */

                    foreach($data['brand'] as $key => $value){
                        $dataMarca['nombre'] = $value;
                        $dataMarca['is_active'] = 1;
                        $searchMarca = $this->_marcaFactory->create()
                        ->addFieldToFilter('nombre', $value);
                        $dataSMarca = $searchMarca->getData();
                        $logger->info("Busqueda de Marca: ". print_r($dataSMarca,true));
                        if(empty($searchMarca)){
                            $marca = $this->_marca->execute($dataMarca);
                        }else{
                            $marca = $dataSMarca[0]['entity_id'];
                        }
                        $dataModelo['id_marca'] = $marca;
                        $dataModelo['nombre'] = $data['modelo'][$key];
                        $dataModelo['is_active'] = 1;
            
                        $searchModelo = $this->_modeloFactory->create()
                        ->addFieldToFilter('nombre', $data['modelo'][$key]);
                        $dataSModelo = $searchModelo->getData();
                        if(empty($dataSModelo)){
                            $modelo = $this->_modelo->execute($dataModelo);
                        }else{
                            $modelo = $dataSModelo[0]['entity_id'];
                        }
            
                       /* if($data['submodelo'][$key]){
                            $dataSubmodelo['id_modelo'] = $modelo;
                            $dataSubmodelo['nombre'] = $data['submodelo'][$key];
                            $dataSubmodelo['is_active'] = 1;
                            $submodelo = $this->_submodelo->execute($dataSubmodelo);
                        }*/
                        $productsRel  = '';
                        foreach($childs as $value){
                            $productsRel .= $value."&";
                        }
                        $productsRel .= $product->getId();
                        
                        $dataMatriz['id_marca'] =  $marca;              
                        $dataMatriz['id_modelo']  = $modelo;                 
                        $dataMatriz['id_submodelo']  = '';                  
                        $dataMatriz['year']  = $data['year'][$key];          
                        $dataMatriz['products']  = $productsRel;
            
            
                        $searchMatriz = $this->_matrizFactory->create()
                        ->addFieldToFilter('id_marca', $marca)
                        //->addFieldToFilter('id_submodelo', $submodelo)
                        ->addFieldToFilter('id_modelo', $modelo)
                        ->addFieldToFilter('year', $data['year'][$key]);
                        
                        $dataSMatriz = $searchMatriz->getData();
                        
                        if(!empty($dataSMatriz)){
                            $logger->info("Matriz de producto: ". print_r($dataSMatriz,true));
                            $sqlRelations = "SELECT * FROM matriz_product_rel WHERE reg_id='".$dataSMatriz[0]['reg_id']."'  ";
                            $relations = $connection->query($sqlRelations);
                            $relSaved = '';
                            foreach($relations as $key => $value){
                                $relSaved .= $value['product_id']."&";
                            }

                            $dataMatriz['reg_id'] = $dataSMatriz[0]['reg_id'];
                            $dataMatriz['products'] = $productsRel."&".$relSaved;
                        }
                        $logger->info("Matriz de guardado: ". print_r($dataMatriz,true));
                        $matriz = $this->_matriz->execute($dataMatriz);  
                    }

                    /** ----- Creando Marcas Inicio ------- */
                }
                /*if($product->save()){
                    $sqlUpdateStatus = "UPDATE " . $tableName . " SET is_active = '3' WHERE entity_id ='".$data['entity_id']."' ";
                    $connection->query($sqlUpdateStatus);
                }*/
            }

        /**  -------- Creacion de producto configurable Fin  ----------- */
        }else{
            /** ------ Creación de Producto Simple Inicio */
            $logger->info('Creando  '.$data['new_title'].' ');
            $product = $objectManager->create('\Magento\Catalog\Model\Product');
            $product->setSku($data['part_number']); 
            $product->setName($data['new_title']); 
            $product->setAttributeSetId(4); 
            $product->setShortDescription($data['new_short_description']); 
            $product->setDescription($data['new_long_description']); 
            $product->setStatus(1); 
            $product->setWeight(10); 
            $product->setCategoryIds($categories);
            $product->setVisibility(4); 
            $product->setWebsiteIds(array(1));
            $product->setTaxClassId(0); 
            $product->setTypeId('simple'); 
            $product->setPrice($data['new_price']); 
            $product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => (($data['new_stock']>0)?1:0),
                    'qty' => $data['new_stock'] 
                )
            );
            if($product->save()){
                /** Función para alimentar matriz Inicio */

                /** Función para alimentar matriz Fin */
            }
            /** ------ Creación de Producto Simple Fin */
           /* if($product->save()){
                $sqlUpdateStatus = "UPDATE " . $tableName . " SET is_active = '3' WHERE entity_id ='".$data['entity_id']."' ";
                $connection->query($sqlUpdateStatus);
            }*/
        }
        

     /*   */
        /* --- Creación de atributos Inicio ---*/

        /* --- Creación de atributos Fin ---*/

        
        /*$imagePath = $data['image_url']; // path of the image
        $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
        $product->save();*/
     }
	}
}