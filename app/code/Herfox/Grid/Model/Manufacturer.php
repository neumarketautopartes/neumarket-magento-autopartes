<?php
/**
 * Herfox_Grid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Manufacturer implements OptionSourceInterface
{
    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $attribute = $om->get(\Magento\Catalog\Api\ProductAttributeRepositoryInterface::class)->get('manufacturer');
        $options = array();
        $options[''] = 'Seleccione';
             if(!empty($attribute->getOptions())){
                 foreach ($attribute->getOptions() as $option) {
                     if($option->getValue()){
                        $options[$option->getValue()] = $option->getLabel();
                     }
                 }
             }
        /*$options = [
        '1' => __('Activo'),
        '0' => __('Desactivado'),
        '2' => __('En proceso'),
        '3' => __('Creado en Magento'),
    ];*/
        return $options;
    }

    /**
     * Get Grid row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Grid row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
