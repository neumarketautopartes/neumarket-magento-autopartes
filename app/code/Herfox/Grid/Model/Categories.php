<?php
/**
 * Herfox_Grid Status Options Model.
 * @category    Herfox
 * @author      Herfox Software Private Limited
 */
namespace Herfox\Grid\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Categories implements OptionSourceInterface
{
    protected $_categoryCollectionFactory;
    protected $_categoryHelper;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
         \Magento\Catalog\Helper\Category $categoryHelper

    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
    }

    /**
     * Get Grid row status type labels array.
     * @return array
     */
    public function getOptionArray()
    {
        $options  = array();
        $options[0] = 'Seleccione';
        $categories = $this->_categoryCollectionFactory->create()                              
        ->addAttributeToSelect('*')
        ->setStore(1);
        foreach ($categories as $category){
            $options[$category->getEntityId()] = $category->getName();
            if($category->hasChildren()) {
                $options[$category->getEntityId()] = '->'.$category->getName();
            }else{
                $options[$category->getEntityId()] = $category->getName();

            }
       }
        return $options;
    }

    /**
     * Get Grid row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Grid row type array for option element.
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
