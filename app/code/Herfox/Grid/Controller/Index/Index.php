<?php
 
namespace Herfox\Grid\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magebees\Finder;
use \Statickidz\GoogleTranslate;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_yumValue;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magento\Eav\Model\Config $eavConfig
    )
    {    
        $this->_yumValue = $yumValue;
        $this->_attributeFactory = $attributeFactory;
        $this->_eavConfig = $eavConfig;
        $this->collectionFactory = $collectionFactory; 
        parent::__construct($context);
    }
    public function strpos_arr($haystack, $needle) {
        foreach($needle as $what) {
            if(strpos($haystack,$what) === false){
                $var = false;
            }else{
                $var = true;
                break;
            }
        }
        return $var;
    }
    public function getUniqueValues($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[$value['name']] = $value;
        }
        return $newArray;
    }
    public function clearArray($data){
        $newArray = array();
        foreach($data as $key => $value){
            $newArray[] = $value;
        }
        return $newArray;
    }
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper-traslate.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('rules');
       $post = $_POST['traducir'];
       $pair = explode('#&nbsp;#',$post);
       /* NOTA: Se debe agregar la clase: para poder enviarlos a este controlador */
       $trans = new GoogleTranslate();
       $source = 'en';
       $target = 'es';
       foreach($pair as $key => $value){
           $valorInput = explode('@&nbsp;@',$value);
          
           if(!empty($value)){
               $reglasTraduccion = array('wpgrid_manufacturer','wpgrid_shipping','wpgrid_categories','wpgrid_attributeset');
               $sql1 = '';
               $sql = '';

               if(in_array($valorInput[0],$reglasTraduccion)){
                $clean1 = $valorInput[1];
                    $sql1 = "SELECT regla FROM rules WHERE valor='".$valorInput[1]."' AND is_active=1 ";
                    $resulSql = $connection->fetchAll($sql1); 
                    if(!empty($resulSql)){
                        $traduccion = $resulSql[0]['regla'];
                        $id = explode('wpgrid_',$valorInput[0]);
                        $newArray[] =  array($id[1],$traduccion);
                    }else{
                        $traduccion = $valorInput[1];
                        $id = explode('wpgrid_',$valorInput[0]);
                        $newArray[] =  array($id[1],$traduccion);
                    }
                    if($valorInput[0]=='wpgrid_attributeset'){
                        $attSet = $traduccion;
                    }
               }else if($this->strpos_arr($valorInput[0],array('wpgrid_marca','wpgrid_modelo','wpgrid_attr','wpgrid_submodelo'))){
                $clean1 = $valorInput[1];
                   $sql = "SELECT regla FROM rules WHERE valor='".$valorInput[1]."' AND is_active=1 ";
                   $resulSql = $connection->fetchAll($sql);
                   if($this->strpos_arr($valorInput[0],array('wpgrid_attr'))){
                       $fieldAtt = explode('_',$valorInput[0]);
                       $sql = "SELECT * FROM rules WHERE attribute_set='".$attSet."' AND valor='".$valorInput[1]."' AND is_active=1 ";
                       $logger->info(print_r($sql,true));
                       $resulSql = $connection->fetchAll($sql);
                            if(isset($resulSql[0]['regla'])){
                                $i = 0;
                                $attributeCollection = $objectManager->get('Magento\Eav\Model\Entity\Attribute')->getCollection();
                                $attributeCollection->setAttributeSetFilter($attSet);
                                foreach($attributeCollection->getData() as $key => $value){
                                    //$logger->info(print_r($value['attribute_code'],true));
                                    $attribute = $this->_eavConfig->getAttribute('catalog_product', $value['attribute_code']);
                                    $optionsAtt = $attribute->getSource()->getAllOptions();
                                    //$logger->info(print_r($optionsAtt[0]['label'],true));
                                    if(!empty($optionsAtt)){
                                        $options['wpgrid_attr_'.$fieldAtt[2]][$i] = array(
                                            'id' => $value['attribute_id'],
                                            'name' => $value['frontend_label'],
                                        );
                                        foreach ($optionsAtt as $keyOption => $valOption) {
                                            if(!empty($valOption['label'])&&!empty($valOption['value'])){
                                            $options['wpgrid_attr_'.$fieldAtt[2]][$i]['options'][] = array(
                                                    'id' => $value['attribute_id'].'-'.$valOption['value'],
                                                    'name' => $valOption['label'],
                                                );
                                            }
                                        }
                                    }
                                    $i++;
                                }
                            $traduccion = $options['wpgrid_attr_'.$fieldAtt[2]];
                            $id = explode('wpgrid_',$valorInput[0]);
                            $logger->info(print_r($id,true));
                            $logger->info(print_r($sql,true));
                            $logger->info(print_r($resulSql,true));
                            $newArray[] =  array($id[1],$traduccion,$resulSql[0]['regla']);
                        }
                   }
                   if($this->strpos_arr($valorInput[0],array('wpgrid_marca'))){
                        $field = explode('_',$valorInput[0]);

                        if(isset($resulSql[0]['regla'])){
                            $finder = $this->_yumValue->create();
                            $finder->addFieldToFilter('ymm_value_id',  $resulSql[0]['regla']);
                            $finderData = $finder->getData();
                            
                            $traduccion = $resulSql[0]['regla'];
                            $id = explode('wpgrid_',$valorInput[0]);
                            $newArray[] =  array($id[1],$traduccion);
                        }
                                $brand = $this->_yumValue->create();
                                $brand->addFieldToFilter('dropdown_id',  6);
                                $brandData = $brand->getData();
                                    foreach($brandData as $key => $value){
                                        $parent = $this->_yumValue->create();
                                        $parent->addFieldToFilter('ymm_value_id',  $value['parent_id']);
                                        $parentData = $parent->getData();
                                       // if($parentData[0]['value']==$charSearch){
                                            $options['wpgrid_modelo_'.$field[2].''][] = array(
                                                'id' => $value['value'],
                                                'name' => $value['value']
                                            );

                                       // }
                                    }
                         }
                       if($this->strpos_arr($valorInput[0],array('wpgrid_modelo'))){

                        $field2 = explode('_',$valorInput[0]);
                        if(isset($resulSql[0]['regla'])){
                            $finder = $this->_yumValue->create();
                            $finder->addFieldToFilter('ymm_value_id',  $resulSql[0]['regla']);
                            $finderData = $finder->getData();
                        }

                        $brand = $this->_yumValue->create();
                        $brand->addFieldToFilter('dropdown_id',  7);
                        $brandData = $brand->getData();
                        
                        foreach($brandData as $key => $value){
                            $parent = $this->_yumValue->create();
                            $parent->addFieldToFilter('ymm_value_id',  $value['parent_id']);
                            $parentData = $parent->getData();
                            
                           // if($parentData[0]['value']==$charSearch){
                                $optSubmodel['wpgrid_submodelo_'.$field2[2].''][] = array(
                                    'id' => $value['value'],
                                    'name' => $value['value']
                                );
                           // }
                        }
                        
                           if(isset($options[$valorInput[0]])){
                               $uniqueValues = $this->clearArray($this->getUniqueValues($options[$valorInput[0]]));
                                $traduccion = $uniqueValues;
                                $id = explode('wpgrid_',$valorInput[0]);
                                if(isset($resulSql[0]['regla'])){
                                     $newArray[] =  array($id[1],$traduccion,$resulSql[0]['regla']);
                                }else{
                                    $newArray[] =  array($id[1],$traduccion,'');
                                }
                           }
                       }
                       if($this->strpos_arr($valorInput[0],array('wpgrid_submodelo'))){
                           if(isset($optSubmodel[$valorInput[0]])){
                                $uniqueValues = $this->clearArray($this->getUniqueValues($optSubmodel[$valorInput[0]]));
                               // $logger->info(print_r($uniqueValues,true));
                                $traduccion = $uniqueValues;
                                $id = explode('wpgrid_',$valorInput[0]);
                                if(isset($resulSql[0]['regla'])){
                                    $newArray[] =  array($id[1],$traduccion,$resulSql[0]['regla']);
                               }else{
                                   if(count($uniqueValues)==1){
                                       $newArray[] =  array($id[1],$traduccion,'NA');
                                    }else{
                                        $newArray[] =  array($id[1],$traduccion,'');
                                   }
                               }
                            }
                        }
                    
                    
                    /*if($valorInput[0]=='wpgrid_marca_1'||$valorInput[0]=='wpgrid_marca_2'||$valorInput[0]=='wpgrid_marca_3'){
                    }else{
                         
                        if(!empty($resulSql)){
                            $traduccion = $resulSql[0]['regla'];
                            $id = explode('wpgrid_',$valorInput[0]);
                            $newArray[] =  array($id[1],$traduccion);
                        }
                    }*/
                    
                        
                    
               }else{
                //$logger->info(print_r($valorInput,true));
                $clean1 = $valorInput[1];
                $traduccion = $trans->translate($source, $target,$clean1);
                $id = explode('wpgrid_',$valorInput[0]);
               $newArray[] =  array($id[1],$traduccion);
               }
            }
        }
        unset($valorInput);
      // echo json_encode($newArray);
    }
}