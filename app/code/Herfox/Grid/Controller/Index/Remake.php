<?php
 
namespace Herfox\Grid\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use \Statickidz\GoogleTranslate;
 
class Remake extends \Magento\Framework\App\Action\Action
{

/**
 * CustomClass constructor.
 *
 * @param \Magento\Framework\App\Action\Context               $context
 */
public function __construct(
    \Magento\Framework\App\Action\Context $context
) {
    parent::__construct($context);
}

    public function execute()
    {
         /**  Insertar datos de prueba  Inicio */
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
         $connection = $resource->getConnection();
         $sql = 'SELECT * FROM products WHERE id>2  ';
         $result = $connection->fetchAll($sql);
         if(date('Y-m-d')=='2018-11-26'){
            foreach($result as $key => $value){
                $sqlAttributes = 'SELECT pro.attribute_id as att_option ,wsatt.attribute_id as att_set FROM products_attributes as pro join webscraper_attributes_options as wsatt on wsatt.id=pro.attribute_id WHERE product_id="'.$value['id'].'" ';
                $resultAttributes = $connection->fetchAll($sqlAttributes);
                if(!empty($resultAttributes)){
                   /* echo '<pre>';
                    print_r($resultAttributes);
                    echo '</pre>';*/
                    $sqlWsAttributes = 'INSERT INTO webscraper_attributes_rel VALUES (NULL,"'.$resultAttributes[0]['att_set'].'","'.$resultAttributes[0]['att_option'].'","'.$value['part_number'].'")';
                    if($connection->query($sqlWsAttributes)){
                        //echo $sqlWsAttributes."<br>";
                       // echo 'Registro correcto de atributos '.print_r($resultAttributes);
                    }
                }

                $sqlVehiculo = 'SELECT vh.brand_id,vh.model_id,vh.submodel_id,vh.year FROM vehicles_products as pro join vehicles as vh on vh.id=pro.vehicle_id WHERE pro.product_id="'.$value['id'].'"';
                $resultVehiculo = $connection->fetchAll($sqlVehiculo);
                foreach($resultVehiculo as $keyBrand => $valueBrand){
                    // Marca
                    $sqlBrand = 'SELECT name FROM brands WHERE id="'.$valueBrand['brand_id'].'" ';
                    $resultBrand = $connection->fetchAll($sqlBrand);
                    // Modelo
                    $sqlModel = 'SELECT name FROM models WHERE id="'.$valueBrand['model_id'].'" ';
                    $resultModel = $connection->fetchAll($sqlModel);
                   
                    // Submodel
                    if(!empty($valueBrand['submodel_id'])){
                        $sqlSubmodel = 'SELECT name FROM submodels WHERE id="'.$valueBrand['submodel_id'].'" ';
                        $resultSubmodel = $connection->fetchAll($sqlSubmodel);
                    }
                    $sqlWsBrands = 'INSERT INTO webscraper_brands VALUES (NULL,"'.$resultBrand[0]['name'].'","'.$resultModel[0]['name'].'","'.$valueBrand['year'].'","'.$value['part_number'].'","'.((isset($resultSubmodel))?$resultSubmodel[0]['name']:'').'")';
                   // echo 'Marca: '.$resultBrand[0]['name']. ' Modelo: '.$resultModel[0]['name'].' Submodelo '.((isset($resultSubmodel))?$resultSubmodel[0]['name']:'').'<br>';
                     if($connection->query($sqlWsBrands)){
                    //echo $sqlWsAttributes."<br>";
                        echo 'Registro correcto de marca modelo año <br>';
                    }
                }
                $value['full_description'] = str_replace('"',"'",$value['full_description']);
               !// die($value['full_description']);
                $value['description'] = str_replace('"','',$value['description']);
                $value['features'] = str_replace('"','',$value['features']);
                $sqlWebscraper = 'INSERT INTO webscraper_products VALUES (
                    NULL,
                    "'.$value['name'].'",
                    "",
                    "",
                    "'.$value['carid_item_number'].'",
                    "",
                    "'.$value['full_description'].'",
                    "",
                    "'.$value['url'].'",
                    "'.$value['oe_numbers'].'",
                    "'.$value['features'].'",
                    "",
                    "",
                    "'.$value['part_number'].'",
                    "",
                    "'.$value['manufacturer'].'",
                    "",
                    "'.$value['price'].'",
                    "",
                    "'.$value['stock'].'",
                    "",
                    "'.$value['shipping'].'",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "'.$value['image_url'].'",
                    "'.$value['description'].'", 
                    "",
                    "",
                    "1",
                    "",
                    "",
                    "'.date('Y-m-d H:i:s').'",
                    "'.date('Y-m-d H:i:s').'"
                    )';
                    if($connection->query($sqlWebscraper)){
                        //echo $sqlWsAttributes."<br>";
                            echo 'Registro correcto de producto<br>';
                        }
            }
        }
        /* echo '<pre>';
         print_r($result);
         echo '</pre>';*/
    }
}