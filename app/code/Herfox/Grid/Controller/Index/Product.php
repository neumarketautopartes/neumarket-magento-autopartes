<?php
 
namespace Herfox\Grid\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use \Statickidz\GoogleTranslate;
 
class Product extends \Magento\Framework\App\Action\Action
{
    private $_save;

/**
 * CustomClass constructor.
 *
 * @param \Magento\Framework\App\Action\Context               $context
 * @param \Custom\Module\Controller\Adminhtml\UpdateData\Save $save
 */
public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Herfox\Grid\Controller\Adminhtml\Grid\Save $save
) {
    parent::__construct($context);
    $this->_save = $save;
}

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectManager->create('\Herfox\Grid\Model\Product');
        $dataFinal = array();
        //$resulSave = $this->_save->execute($_POST['groups']);
        /*if($resulSave='success'){
        }*/
        $data =  $model->createProduct(json_decode($_POST['groups'], true));
       echo json_encode('completado');
    }
}