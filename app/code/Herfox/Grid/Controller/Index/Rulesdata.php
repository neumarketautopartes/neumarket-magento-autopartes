<?php

namespace Herfox\Grid\Controller\Index;

use Magento\Framework\App\Action\Context;

class Rulesdata extends \Magento\Framework\App\Action\Action
{
    protected $_categoryCollectionFactory;
    protected $_categoryHelper;
    protected $_attributeFactory;
    protected $_attributeGroupFactory;
    protected $_mapValue;
    protected $_yumValue;
    protected $_dropDonw;

/**
 * CustomClass constructor.
 *
 * @param \Magento\Framework\App\Action\Context               $context
 */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magebees\Finder\Model\ResourceModel\Mapvalue\CollectionFactory $mapValue,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory $attributeGroupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory,
        \Magebees\Finder\Model\ResourceModel\Ymmvalue\CollectionFactory $yumValue,
        \Magento\Eav\Model\Config $eavConfig,
        \Magebees\Finder\Model\ResourceModel\Dropdowns\CollectionFactory $dropDonw

    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->_attributeFactory = $attributeFactory;
        $this->_attributeGroupFactory = $attributeGroupFactory;
        $this->_mapValue = $mapValue;
        $this->_eavConfig = $eavConfig;
        $this->_yumValue = $yumValue;
        $this->_dropDonw = $dropDonw;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_rulesdata.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        if (!empty($_POST)) {

            $om = \Magento\Framework\App\ObjectManager::getInstance();

            $options = array();
            /* Categorias de producto Inicio*/
            if ($_POST['data']['rule'] == 1 && $_POST['data']['tipodato'] == 7) {
                $categories = $this->_categoryCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->setStore(1);
                $categoryById = array();
                foreach ($categories as $category) {
                    $logger->info(print_r($category->getData(), true));

                    // if($category->getEntityId() != 1 && $category->getEntityId() != 2){
                    $str = '';
                    /* $position = $category->getPosition();
                    $name = str_pad($str,$position,"-",STR_PAD_LEFT).$category->getName();*/
                    $options[] = array(
                        'id' => $category->getEntityId(),
                        'name' => $category->getName(),
                        'parent' => $category->getParentId(),
                    );
                    // }
                }
            }
            /* Categorias de producto Fin*/

            /* Fabricante Inicio */
            if ($_POST['data']['rule'] == 2 && $_POST['data']['tipodato'] == 1) {
                $attribute = $om->get(\Magento\Catalog\Api\ProductAttributeRepositoryInterface::class)->get('manufacturer');
                $options = array();
                if (!empty($attribute->getOptions())) {
                    foreach ($attribute->getOptions() as $option) {
                        if ($option->getValue()) {
                            $options[] = array(
                                'id' => $option->getValue(),
                                'name' => $option->getLabel(),
                            );
                        }
                    }
                }
            }
            /* Fabricante Fin */

            /* Conjunto de atributos Inicio */
            if ($_POST['data']['rule'] == 2 && $_POST['data']['tipodato'] == 7) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                if(isset($_POST['data']['type']) == 'formIndex'){
                    $_POST['data']['attributeset'] = $_POST['data']['value'];
                }
                    $attSet = $_POST['data']['attributeset'];
                
               
               /* $attributeSetId = $attSet; //your_attributeSetId
                $attributeSet = $objectManager->create('Magento\Eav\Api\AttributeSetRepositoryInterface');
                $attributeSetRepository = $attributeSet->get($attributeSetId);*/

                /*$attribute_set_name = $attributeSetRepository->getAttributeSetName();
                $attributeGroupCollection = $objectManager->get('Magento\Eav\Model\Entity\Attribute\Group')->getCollection();
                $attributeGroupCollection->addFieldToFilter('attribute_set_id', $attSet);*/

                
                $i = 0;
                $attributeCollection = $objectManager->get('Magento\Eav\Model\Entity\Attribute')->getCollection();
                $attributeCollection->setAttributeSetFilter($attSet);
                foreach($attributeCollection->getData() as $key => $value){
                    //$logger->info(print_r($value['attribute_code'],true));
                    $attribute = $this->_eavConfig->getAttribute('catalog_product', $value['attribute_code']);
                    $optionsAtt = $attribute->getSource()->getAllOptions();
                    //$logger->info(print_r($optionsAtt[0]['label'],true));
                    if(!empty($optionsAtt)){
                        $options[$i] = array(
                            'id' => $value['attribute_id'],
                            'name' => $value['frontend_label'],
                        );
                        foreach ($optionsAtt as $keyOption => $valOption) {
                            if(!empty($valOption['label'])&&!empty($valOption['value'])){
                            $options[$i]['options'][] = array(
                                    'id' => $value['attribute_id'].'-'.$valOption['value'],
                                    'name' => $valOption['label'],
                                );
                            }
                        }
                    }
                    $i++;
                }
                //$logger->info(print_r($options,true));

                 
                if (empty($_POST['data']['attributeset'])) {
                    $options = array();
                }

            }
            /* Conjunto de atributos Fin */

            /* Vehiculo Marca Inicio */
            if ($_POST['data']['rule'] == 3 && $_POST['data']['tipodato'] == 8) {
                $marcas = $this->_yumValue->create();
                $marcas->addFieldToFilter('dropdown_id', 5);
                $marcasData = $marcas->getData();
                foreach ($this->getUniqueValues($marcasData) as $key => $value) {
                    $options[] = array(
                        'id' => $value['value'],
                        'name' => $value['value'],
                    );
                }
                //$logger->info(print_r($marcasData,true));
            }
            /* Vehiculo Marca Fin */

            /* Vehiculo Modelo Inicio */
            if ($_POST['data']['rule'] == 3 && $_POST['data']['tipodato'] == 9) {
                $modelos = $this->_yumValue->create();
                $modelos->addFieldToFilter('dropdown_id', 6);
                $modelosData = $modelos->getData();
                foreach ($this->getUniqueValues($modelosData) as $key => $value) {
                    $options[] = array(
                        'id' => $value['value'],
                        'name' => $value['value'],
                    );
                }
            }
            /* Vehiculo Modelo Fin */

            /* Vehiculo Submodelo Inicio */
            if ($_POST['data']['rule'] == 3 && $_POST['data']['tipodato'] == 10) {
                $submodelos = $this->_yumValue->create();
                $submodelos->addFieldToFilter('dropdown_id', 7);
                $submodelosData = $submodelos->getData();
                foreach ($this->getUniqueValues($submodelosData) as $key => $value) {
                    $options[] = array(
                        'id' => $value['value'],
                        'name' => $value['value'],
                    );
                }
            }
            if ($_POST['data']['rule'] == 1000 && $_POST['data']['tipodato'] == 2000) {
                if (!empty($_POST['data']['value'])) {
                    $index = $this->_yumValue->create();
                    $index->addFieldToFilter('ymm_value_id', $_POST['data']['value']);
                    $indexData = $index->getData();
                    //  echo $indexData[0]['value'];

                    $brand = $this->_yumValue->create();
                    $brand->addFieldToFilter('dropdown_id', $_POST['data']['dropdownnext']);
                    $brandData = $brand->getData();
                    //foreach($this->getUniqueValues($brandData) as $key => $value){
                    foreach ($brandData as $key => $value) {
                        $parent = $this->_yumValue->create();
                        $parent->addFieldToFilter('ymm_value_id', $value['parent_id']);
                        $parentData = $parent->getData();
                        //if ($parentData[0]['value'] == $indexData[0]['value']) {
                            $options[] = array(
                                'id' => $value['value'],
                                'name' => $value['value'],
                            );
                       // }
                    }
                    $inuque = $this->getUniqueValues2($options);
                    /*echo '<pre>';
                    print_r($inuque );
                    echo '</pre>';*/
                    $options = $this->clearArray($inuque);
                }
            }
            if ($_POST['data']['rule'] == 2 && $_POST['data']['tipodato'] == 11) {
                $attributeSetCollection = $this->collectionFactory->create();
                $attributeSets = $attributeSetCollection->getItems();
                $attributeNoAcepted = array(1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
                foreach ($attributeSets as $attributeSet) {
                    if (!in_array($attributeSet->getAttributeSetId(), $attributeNoAcepted)) {
                        $options[] = array(
                            'id' => $attributeSet->getAttributeSetId(),
                            'name' => $attributeSet->getAttributeSetName(),
                        );
                    }
                }
            }
            /* Vehiculo Submodelo Fin */
            echo json_encode($options);
        }
    }
    public function getUniqueValues2($data)
    {
        $newArray = array();
        foreach ($data as $key => $value) {
            $newArray[$value['name']] = $value;
        }
        return $newArray;
    }
    public function getUniqueValues($data)
    {
        $newArray = array();
        foreach ($data as $key => $value) {
            $newArray[$value['value']] = $value;
        }
        return $newArray;
    }
    public function clearArray($data)
    {
        $newArray = array();
        foreach ($data as $key => $value) {
            $newArray[] = $value;
        }
        return $newArray;
    }
}
