<?php
/**
 * Grid Record Index Controller.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Controller\Adminhtml\Grid;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    protected $_gridFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Herfox\Grid\Model\GridFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Herfox\Grid\Model\GridFactory $gridFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_gridFactory = $gridFactory;
    }

    /**
     * Mapped eBay Order List page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $grid = $this->_gridFactory->create();
		$collection = $grid->getCollection();
        $collection->addFieldToFilter('parent', '');
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Herfox_Grid::grid_list');
        $resultPage->getConfig()->getTitle()->prepend(__('Listado de productos'));
        
       /* foreach($collection as $item){
			echo "<pre>";
			print_r($item->getData());
			echo "</pre>";
        }
        exit();*/
        return $resultPage;
    }

    /**
     * Check Order Import Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Grid::grid_list');
    }
}
