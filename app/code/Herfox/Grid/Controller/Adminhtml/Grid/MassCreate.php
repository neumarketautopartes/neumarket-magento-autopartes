<?php
/**
 * Herfox Grid Record Delete Controller.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Controller\Adminhtml\Grid;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Herfox\Grid\Model\ResourceModel\Grid\CollectionFactory;
use \Statickidz\GoogleTranslate;

class MassCreate extends \Magento\Backend\App\Action
{
    /**
     * Massactions filter.
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param Context           $context
     * @param Filter            $filter
     * @param CollectionFactory $collectionFactory
     */ 
    public function __construct(
        Context $context, 
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Herfox\Grid\Model\Product $product,
        \Herfox\Grid\Model\MasiveData $massiveData,
        \Herfox\Grid\Model\CreateMagentoValue $createMagentoValue,
        \Magebees\Finder\Controller\Adminhtml\Product\Save $finderProduct,
        \Herfox\Grid\Model\ResourceModel\Rule\CollectionFactory $rules,
        \Herfox\Grid\Model\Rulesave $rulesave,
        \Herfox\Grid\Model\MassiveProduct $massiveProduct
    ) {

        $this->_filter = $filter;
        $this->_product = $product;
        $this->_createMagentoValue = $createMagentoValue;
        $this->_rules = $rules;
        $this->_massiveData = $massiveData;
        $this->_collectionFactory = $collectionFactory;
        $this->rulesave = $rulesave;
        $this->_finderProduct = $finderProduct;
        $this->_massiveProduct = $massiveProduct;
        parent::__construct($context);
    }

    /**
     * 
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $date = date("Y-m-d");
        $trans = new GoogleTranslate();
        $source = 'en';
       $target = 'es';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_masive_product.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $recordDeleted = 0;
        foreach ($collection->getItems() as $record) {
            if($record->getIsActive()=='2'||$record->getIsActive()=='1'){
                $dataToCreate = $record->getData();
                // Categoria ------------------------------------

                $getRuleCategori = $this->getRule('Disc Brake Pads',1,7);
                $geEnvio = $this->getRule($dataToCreate['shipping'],4,7);
                $dataToCreate['new_shipping'] = $geEnvio[0]['regla'];
                
                if($getRuleCategori){
                    $dataToCreate['new_categories'] = $getRuleCategori[0]['regla']; 
                    $dataToCreate['categories'] = 'Disc Brake Pads';
                }else{
                    $dataToCreate['new_categories'] = 67; // Valor creado por $this->createMagentoValue()->Create();
                    $dataToCreate['categories'] = 'Disc Brake Pads';
                }
                $this->rulesave->saveRule('category',array(
                    'new_categories' => $getRuleCategori[0]['regla'],
                    'categories' => 'Disc Brake Pads',
                ));
                // Categoria ------------------------------------

                // Conjunto de attributos ------------------------------------------
                $getRuleAttSet = $this->getRule('Brake Pads',2,11);
                if($getRuleAttSet){
                    $attSetAsignado = $getRuleAttSet[0]['regla'];
                    $dataToCreate['attributeset'] = 'Brake Pads';
                    $dataToCreate['new_attributeset'] = $attSetAsignado; 
                }else{
                    $attSetAsignado = 28;
                    $dataToCreate['attributeset'] = 'Brake Pads';
                    $dataToCreate['new_attributeset'] = $attSetAsignado; // Valor creado por $this->createMagentoValue()->Create();
                }
                $this->rulesave->saveRule('attributeset',array(
                    'attributeset' => 'Brake Pads',
                    'new_attributeset' => $attSetAsignado
                ));
                // Conjunto de attributos ------------------------------------------

                // Fabricante  ------------------------------------------
                $getFabricante = $this->getRule($record->getManufacturer(),2,1);
                if($getFabricante){
                    $fabSignado = $getFabricante[0]['regla'];
                    $dataToCreate['manufacturer'] = $record->getManufacturer(); 
                    $dataToCreate['new_manufacturer'] = $fabSignado; 
                }else{
                    $fabSignado = 22;
                    $dataToCreate['manufacturer'] = $record->getManufacturer(); 
                    $dataToCreate['new_manufacturer'] = $fabSignado; // Valor creado por $this->createMagentoValue()->Create();
                }
                $this->rulesave->saveRule('manufacturer',array(
                    'manufacturer' => $record->getManufacturer(),
                    'new_manufacturer' => $fabSignado
                ));
                // Fabricante  ------------------------------------------

                
                $sqlAttributes = 'SELECT * FROM webscraper_attributes_rel AS rel JOIN webscraper_attributes AS attr ON rel.attribute_id=attr.id  WHERE rel.part_number="'.$record->getPartNumber().'" ';
                $resultAttributes = $connection->fetchAll($sqlAttributes); 
                foreach($resultAttributes as $attrId => $atrr){
                    $sqlOptios = "SELECT * FROM webscraper_attributes_options WHERE id='".$atrr['option_id']."' ";
                    $resultoptions = $connection->fetchAll($sqlOptios);
                    // Opcion de attributo  ------------------------------------------
                    $getAttOption = $this->getRule($resultoptions[0]['nombre'],2,7,$attSetAsignado);
                    if($getAttOption){
                        $optionAttAsignado = $getAttOption[0]['regla'];

                        $dataToCreate['new_attr_'][$record->getPartNumber()][] = $optionAttAsignado;
                        $dataToCreate['attr_'][$record->getPartNumber()][] = $resultoptions[0]['nombre'];
                        $arrayOptionsAtt['new_attr_'][$record->getPartNumber()][] = $optionAttAsignado;
                        $arrayOptionsAtt['attr_'][$record->getPartNumber()][] = $resultoptions[0]['nombre'];
                        $arrayOptionsAtt['new_attributeset'] = $attSetAsignado;

                    }else{
                        $optionAttAsignado = 312;
                        $dataToCreate['new_attr_'][$record->getPartNumber()][] = $optionAttAsignado;
                        $dataToCreate['attr_'][$record->getPartNumber()][] = $resultoptions[0]['nombre']; 
                        $arrayOptionsAtt['new_attr_'][$record->getPartNumber()][] = $optionAttAsignado;
                        $arrayOptionsAtt['attr_'][$record->getPartNumber()][] = $resultoptions[0]['nombre'];
                        $arrayOptionsAtt['new_attributeset'] = $attSetAsignado;
                    }
                     // Opcion de attributo   ------------------------------------------
                }
                $this->rulesave->saveRule('attributes',$arrayOptionsAtt);

                // Marcas
                $sqlBrandsParent = "SELECT * FROM webscraper_brands WHERE part_number='".$record->getPartNumber()."'";
                $resultBrandsParent = $connection->fetchAll($sqlBrandsParent);
                
                foreach($resultBrandsParent as $keyRel => $valRel){
                    $dataToCreate['year_show'][] = $valRel['year'];
                    $dataToCreate['brand'][] = $valRel['marca'];
                    $dataToCreate['marca_old'][] = $valRel['marca'];
                    $dataToCreate['modelo'][] = $valRel['modelo'];
                    $dataToCreate['submodelo'][] = ((!empty($valRel['submodelo']))?$valRel['submodelo']:'NA');
                    $dataToCreate['submodelo_old'][] = ((!empty($valRel['submodelo']))?$valRel['submodelo']:'NA');
                }
                $this->rulesave->saveRule('brands',$dataToCreate);
                $dataToCreate['new_short_description'] = $trans->translate($source, $target,$dataToCreate['short_description']);
                $dataToCreate['new_long_description'] = $trans->translate($source, $target,$dataToCreate['long_description']);
                $dataToCreate['new_title'] = $trans->translate($source, $target,$dataToCreate['title']);
                $dataToCreate['new_stock'] = intval(preg_replace('/[^0-9]+/', '', $dataToCreate['stock']), 10);
                $soapClient = new \Zend\Soap\Client('https://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService?WSDL');
                $soapClient->setSoapVersion(SOAP_1_1);
                $soapClient->setLocation('http://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService');
                //$logger->info(print_r($date,true));
                $response = $soapClient->queryTCRM(array('tcrmQueryAssociatedDate' => $date));
                $trm = '';
                if($response->return->success){
                  $trm = $response->return->value;
                }
                $priceCID = $dataToCreate['price'];
                $precioFinal = ($trm * $priceCID) + 50000;
                $dataToCreate['new_price'] = $precioFinal;
                
               $logger->info(print_r($dataToCreate,true));
               $this->_massiveProduct->createProduct($dataToCreate);
            $recordDeleted++;
            
          }
        }
        $this->messageManager->addSuccess(__('Se han creado %1 productos.', $recordDeleted));

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('*/*/index');
    }
    public function getRule($valor,$ruleType,$dataType,$attSet = ''){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_masive_product.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $rules = $this->_rules->create();
        $rules->addFieldToFilter('valor',  $valor);
        $rules->addFieldToFilter('rule_type',  $ruleType);
        $rules->addFieldToFilter('tipo_dato',  $dataType);
        if(!empty($attSet)){
            $rules->addFieldToFilter('attribute_set',  $attSet);
        }
        $logger->info(print_r($rules->getSelect()->__toString(),true));
        $rulesData = $rules->getData();
        if(!empty($rulesData)){
            return $rulesData;
        }else{
            return false;
        }
    }

    /**
     * Check Category Map recode delete Permission.
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Grid::row_data_delete');
    }
}
