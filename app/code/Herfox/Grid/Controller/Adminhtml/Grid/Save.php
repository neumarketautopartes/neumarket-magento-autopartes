<?php

/**
 * Grid Admin Cagegory Map Record Save Controller.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2016 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Controller\Adminhtml\Grid;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Herfox\Grid\Model\GridFactory
     */
    var $gridFactory;
    protected $rulesave;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Herfox\Grid\Model\GridFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Herfox\Grid\Model\GridFactory $gridFactory,
        \Herfox\Grid\Model\Rulesave $rulesave
    ) {
        parent::__construct($context);
        $this->gridFactory = $gridFactory;
        $this->rulesave = $rulesave;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute($dataAjax = '')
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_in_save.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        $data = $this->getRequest()->getPostValue();
        $data['is_active'] = 2;
        if(!empty($dataAjax)){
            $data = $dataAjax;
        }
        //$logger->info($data);
        if (!$data) {
            $this->_redirect('grid/grid/addrow');
            return;
        }
        try {
            $rowData = $this->gridFactory->create();
            if(!empty($data['new_manufacturer'])){
                $this->rulesave->saveRule('manufacturer',$data); // Verificado
            }
            if(!empty($data['new_categories'])){
                $this->rulesave->saveRule('category',$data); // Verificado
            }
            $this->rulesave->saveRule('brands',$data); // Verificado
            $this->rulesave->saveRule('attributeset',$data); // Verificado
            $this->rulesave->saveRule('attributes',$data);
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setEntityId($data['id']);
            }
            $rowData->save();
            $this->messageManager->addSuccess(__('Registro actualizado.'));
            if(!empty($dataAjax)){
                return  'success';
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        
        $this->_redirect('grid/grid/index');
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Grid::save');
    }
}
