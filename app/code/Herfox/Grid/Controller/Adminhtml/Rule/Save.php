<?php

/**
 * Grid Admin Cagegory Map Record Save Controller.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2016 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Controller\Adminhtml\Rule;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Herfox\Grid\Model\RuleFactory
     */
    var $gridFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Herfox\Grid\Model\RuleFactory $ruleFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Herfox\Grid\Model\RuleFactory $ruleFactory
    ) {
        parent::__construct($context);
        $this->ruleFactory = $ruleFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/rules_in_save.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('grid/rule/addrow');
            return;
        }
        try {
            $rowData = $this->ruleFactory->create();
            $rowData->setData($data);
            $logger->info(print_r($data,true));
            if (isset($data['id'])) {
                $rowData->setEntityId($data['id']);
            }
            $rowData->save();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('grid/rule/index');
        
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Grid::save');
    }
}
