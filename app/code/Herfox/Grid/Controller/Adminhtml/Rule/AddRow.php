<?php
/**
 * Herfox Grid List Controller.
 * @category  Herfox
 * @package   Herfox_Grid
 * @author    Herfox
 * @copyright Copyright (c) 2010-2017 Herfox Software Private Limited (https://herfox.com)
 * @license   https://store.herfox.com/license.html
 */
namespace Herfox\Grid\Controller\Adminhtml\Rule;

use Magento\Framework\Controller\ResultFactory;

class AddRow extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Herfox\Grid\Model\RuleFactory
     */
    private $ruleFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \Herfox\Grid\Model\RuleFactory $ruleFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Herfox\Grid\Model\RuleFactory $ruleFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->ruleFactory = $ruleFactory;
    }

    /**
     * Mapped Grid List page.
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rowId = (int) $this->getRequest()->getParam('id');
        $rowData = $this->ruleFactory->create();
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $rowTitle = $rowData->getTitle();
            if (!$rowData->getEntityId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('grid/rule/rowdata');
                return;
            }
        }

        $this->coreRegistry->register('row_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Editar').$rowTitle : __('Agregar');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Herfox_Grid::add_row');
    }
}
