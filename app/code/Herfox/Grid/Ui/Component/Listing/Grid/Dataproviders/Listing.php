<?php
namespace Herfox\Grid\Ui\Component\Listing\Grid\DataProviders;

class Listing extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Herfox\Grid\Model\ResourceModel\Grid\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $grid = $collectionFactory->create()
        ->addFieldToFilter('parent', '');
        $this->collection = $grid;
    }
}