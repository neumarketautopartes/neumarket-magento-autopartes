<?php
/**
 * Herfox_Grid Add New Row Form Admin Block.
 * @category    Herfox
 * @package     Herfox_Grid
 * @author      Herfox Software Private Limited
 *
 */
namespace Herfox\Grid\Block\Adminhtml\Rule\Edit;
use \Herfox\Grid\Helper\Data;
/**
 * Adminhtml Add New Row Form.
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    public $_storeManager;

    /**
     * @param \Magento\Backend\Block\Template\Context $context,
     * @param \Magento\Framework\Registry $registry,
     * @param \Magento\Framework\Data\FormFactory $formFactory,
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
     * @param \Herfox\Grid\Model\Status $options,
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Herfox\Grid\Model\Status $options,
        \Herfox\Grid\Model\StatusRule $statusrule,
        \Herfox\Grid\Model\RuleType $ruletype,
        \Herfox\Grid\Model\Tipodato $tipodato,
        \Herfox\Grid\Model\AttributeSet $attributeset,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Herfox\Grid\Model\RulesExtra $extra,
        array $data = [],
        Data $helper
    ) {
        $this->_statusrule = $statusrule;
        $this->_options = $options;
        $this->_tipodato = $tipodato;
        $this->_attributeset = $attributeset;
        $this->_ruletype = $ruletype;
        $this->_extra = $extra;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_storeManager=$storeManager;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {

        $dateFormat = 'yyyy/mm/dd';
        $model = $this->_coreRegistry->registry('row_data');
        $dataExtra = $this->getDataArray($model->getRuleType(),$model->getTipoDato());
        $form = $this->_formFactory->create(
            ['data' => [
                            'id' => 'edit_form',
                            'enctype' => 'multipart/form-data',
                            'action' => $this->getData('action'),
                            'method' => 'post'
                        ]
            ]
        );

        $form->setHtmlIdPrefix('wpgrid_');
        if ($model->getEntityId()) {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Editar'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Agregar'), 'class' => 'fieldset-wide']
            );
        }
       
$fieldset->addField(
    'valor',
    'text',
    [
        'name' => 'valor',
        'label' => __('Valor'),
        'id' => 'valor',
        'title' => __('Valor'),
        'class' => 'required-entry',
        'required' => true,
        ]
    );
    $fieldset->addField(
        'rule_type',
        'select',
        [
            'name' => 'rule_type',
            'label' => __('Tipo de Regla'),
            'id' => 'rule_type',
            'title' => __('Tipo de Regla'),
            'values' => $this->_ruletype->getOptionArray(),
            'class' => 'status required-entry',
            'required' => true,
        ]
    );
    $fieldset->addField(
        'tipo_dato',
        'select',
        [
            'name' => 'tipo_dato',
            'label' => __('Tipo de relación'),
            'id' => 'tipo_dato',
            'title' => __('Tipo de relación'),
            'values' => $this->_tipodato->getOptionArray(),
            'class' => 'status required-entry',
            'required' => true,
        ]
    );
    $cat = $form->getElement('tipo_dato');
    $cat->setAfterElementHtml('
    <style>
    .field-attribute_set{
        display: none;
    }
    </style>
    ');

    $fieldset->addField(
        'attribute_set',
        'select',
        [
            'name' => 'attribute_set',
            'label' => __('Grupo  de Atributos'),
            'id' => 'attribute_set',
            'title' => __('Grupo de Atributos'),
            'values' => $this->_attributeset->getOptionArray(),
            'class' => 'status',
        ]
    );


    $fieldset->addField(
        'regla',
        'select',
        [
            'name' => 'regla',
            'label' => __('Regla de cambio'),
            'id' => 'regla',
            'title' => __('Regla de cambio'),
            'class' => 'required-entry',
            'required' => true,
            ]
        );
     
    $fieldset->addField(
        'is_active',
        'select',
        [
            'label' => __('Estatus'),
            'name' => 'is_active',
            'id' => 'is_active',
            'title' => __('Estatus'),
            'values' => $this->_statusrule->getOptionArray(),
            'class' => 'status required-entry',
            'required' => true,
        ]
    );
    $fieldset->addField(
        'fieldsep2',
        'hidden',
        [
            'name' => 'fieldsep2',
            'id' => 'fieldsep2',
            ]
        );
        $siteUrl = $this->_storeManager->getStore()->getBaseUrl();
        $ajax = $form->getElement('fieldsep2');
        $ajax->setAfterElementHtml('<script type="text/javascript"> 
        require(["jquery", "jquery/ui"], function($){ 
            $(document).ready(function () {
                /* Mostrar Conjunto de attributos Inicio */
                $("#wpgrid_tipo_dato").change(function(){
                    if($("#wpgrid_rule_type").val()=="2"&&$(this).val()==7){
                        $(".field-attribute_set").show();
                    }
                });
                /* Mostrar Conjunto de attributos Fin */
                /** Rules Data Inicio */
                    $("#wpgrid_tipo_dato").change(function(){
                        if($(this).val()!=7){
                            loadData();
                        }
                    });
                    $("#wpgrid_attribute_set").change(function(){
                        loadData();
                    });
                        function loadData(){
                            var params = $("#wpgrid_rule_type").val();
                            $("#msgLoaderHf").html("Cargando");
                            $(".loading-mask-HF").show();
                            $.ajax({
                                method: "POST",
                                url: "'.$siteUrl.'grid/index/rulesdata",
                                data: { data: {rule:$("#wpgrid_rule_type").val(),tipodato:$("#wpgrid_tipo_dato").val(),attributeset:$("#wpgrid_attribute_set").val()}},
                                dataType: "json"
                                })
                            .done(function( msg ) {
                                
                                if($("#wpgrid_tipo_dato").val()==7&&$("#wpgrid_rule_type").val()==2){
                                    console.log(msg);
                                    $(msg).each(function( index,value ) {
                                        options += "<optgroup label=\""+ value.name +"\">";
                                        $(value.options).each(function( optionId,optionValue ) {
                                            options += "<option value=\""+ optionValue.id +"\">"+optionValue.name +"</option>";
                                        })
                                        options += "</optgroup>";
                                    });
                                }else if(($("#wpgrid_tipo_dato").val()==8&&$("#wpgrid_rule_type").val()==3)||($("#wpgrid_tipo_dato").val()==9&&$("#wpgrid_rule_type").val()==3)||($("#wpgrid_tipo_dato").val()==10&&$("#wpgrid_rule_type").val()==3)){
                                    var options = "<option value=\"\">Seleccione</option>";
                                    $(msg).each(function( index,value ) {
                                        options += "<option value=\""+ value.name +"\">"+value.name +"</option>";
                                        console.log(value);
                                    });
                                }else{
                                    var options = "<option value=\"\">Seleccione</option>";
                                    $(msg).each(function( index,value ) {
                                        options += "<option value=\""+ value.id +"\">"+value.name +"</option>";
                                        console.log(value);
                                    });
                                }
                                $("#wpgrid_regla").html(options);
                                $(".loading-mask-HF").hide();
                            });
                        }
                    /** Rules Data Fin */
            });
        });
        </script><div style="display:none" class="loading-mask loading-mask-HF" data-role="loader">
        <div class="loader">
            <p id="msgLoaderHf" ></p>
            </div>
        </div>');

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
