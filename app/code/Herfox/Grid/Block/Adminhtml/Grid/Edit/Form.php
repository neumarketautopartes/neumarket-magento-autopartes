<?php
/**
 * Herfox_Grid Add New Row Form Admin Block.
 * @category    Herfox
 * @package     Herfox_Grid
 * @author      Herfox Software Private Limited
 *
 */
namespace Herfox\Grid\Block\Adminhtml\Grid\Edit;
use \Herfox\Grid\Helper\Data;
/**
 * Adminhtml Add New Row Form.
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Herfox\Grid\Model\Status $options
     * @param \Herfox\Grid\Model\Estra $extra
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Herfox\Grid\Model\Status $options,
        \Herfox\Grid\Model\Manufacturer $manufacturers,
        \Herfox\Grid\Model\Categories $categories,
        \Herfox\Grid\Model\Extra $extra,
        \PHPCuong\CategoryTree\Model\Config\Source\CategoryTree $categoryTree,
        array $data = [],
        Data $helper
    ) {
        $this->_categorytree = $categoryTree;
        $this->_categories = $categories;
        $this->_manufacturers = $manufacturers;
        $this->_options = $options;
        $this->_extra = $extra;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/webscraper_from_ws.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $dateFormat = 'yyyy/mm/dd';
        $model = $this->_coreRegistry->registry('row_data');
       $dataExtra = $this->_extra->getDataArray($model->getPartNumber()); 
        
        $form = $this->_formFactory->create(
            ['data' => [
                            'id' => 'edit_form',
                            'enctype' => 'multipart/form-data',
                            'action' => $this->getData('action'),
                            'method' => 'post'
                        ]
            ]
        );

        $form->setHtmlIdPrefix('wpgrid_');
        if ($model->getEntityId()) {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Editar'), 'class' => 'fieldset-wide']
            );
            //$model->setData('categories', $dataExtra['category']);
            $model->setData('categories', 'Disc Brake Pads');
            $model->setData('url_carid', $model->getUrk());
            $model->setData('stock', intval(preg_replace('/[^0-9]+/', '', $model->getStock()), 10));
          $date = date("Y-m-d");
          
          $soapClient = new \Zend\Soap\Client('https://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService?WSDL');
          $soapClient->setSoapVersion(SOAP_1_1);
          $soapClient->setLocation('http://www.superfinanciera.gov.co/SuperfinancieraWebServiceTRM/TCRMServicesWebService/TCRMServicesWebService');
          //$logger->info(print_r($date,true));
          $response = $soapClient->queryTCRM(array('tcrmQueryAssociatedDate' => $date));
          $trm = '';
          if($response->return->success){
            $trm = $response->return->value;
          }
          $priceCID = $model->getPrice();
          $precioFinal = ($trm * $priceCID) + 50000;
          $model->setData('new_price', $precioFinal);
          $logger->info(print_r($response,true));
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Agregar'), 'class' => 'fieldset-wide']
            );
        }
        /** Nombre producto Inicio */
        $fieldset->addField(
            'img_product',
            'hidden',
            [
                'name' => 'img_product',
                'id' => 'img_product',
                ]
            );
            $img = $form->getElement('img_product');
            $img->setAfterElementHtml('<img src="'.$model->getImageUrl().'" style="width:140px;" >
            <br> TRM: '.((!empty($trm))?number_format($trm,2,',','.'):'').' - Calculo de precio: PRECIO USD * TRM + 50.000 COP');


$fieldset->addField(
    'title',
    'text',
    [
        'name' => 'title',
        'label' => __('Nombre Origal'),
        'id' => 'title',
        'readonly' => 'readonly',
         'class' => 'traslate-text-hfx',
        'title' => __('Nombre Origal'),
        'style' => 'background-color: #dcdcdc;',
        ]
    );
    $fieldset->addField(
        'new_title',
        'text',
        [
            'name' => 'new_title',
            'label' => __('Nombre'),
            'id' => 'new_title',
            'title' => __('Nombre'),
            'class' => 'required-entry',
            'required' => true,
            ]
        );
    $fieldset->addField(
        'url_carid',
        'hidden',
        [
            'name' => 'url_carid',
            'id' => 'url_carid',
            ]
        );
        /** Nombre producto Fin */
        /** Part Number Inicio */
        $fieldset->addField(
            'part_number',
            'text',
            [
                'name' => 'part_number',
                'label' => __('Part Number Origal'),
                'id' => 'part_number',
                'readonly' => 'readonly',
                'class' => 'copy-data',
                'title' => __('part Number Origal'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_part_number',
                'text',
                [
                    'name' => 'new_part_number',
                    'label' => __('Part Number'),
                    'id' => 'new_part_number',
                    'title' => __('Part Number'),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
        /** Part Number Fin */
        /** Fabricante Inicio */
        $fieldset->addField(
            'manufacturer',
            'text',
            [
                'name' => 'manufacturer',
                'label' => __('Fabricante Original'),
                'id' => 'manufacturer',
                'readonly' => 'readonly',
                'class' => 'traslate-text-hfx',
                'title' => __('Fabricante Original'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_manufacturer',
                'select',
                [
                    'name' => 'new_manufacturer',
                    'label' => __('Fabricante'),
                    'id' => 'new_manufacturer',
                    'title' => __('Fabricante'),
                    'values' => $this->_manufacturers->getOptionArray(),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
        /** Fabricante Fin */
        /** Precio Inicio */
        $fieldset->addField(
            'price',
            'text',
            [
                'name' => 'price',
                'label' => __('Precio Original'),
                'id' => 'price',
                'class' => 'copy-data',
                'readonly' => 'readonly',
                'title' => __('Precio Original'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_price',
                'text',
                [
                    'name' => 'new_price',
                    'label' => __('Precio'),
                    'id' => 'new_price',
                    'title' => __('Precio'),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
        /** Precio Final */
        /** Inventario Inicio */
        $fieldset->addField(
            'stock',
            'text',
            [
                'name' => 'stock',
                'label' => __('Stock Original'),
                'id' => 'stock',
                'readonly' => 'readonly',
                'class' => 'copy-data',
                'title' => __('Stock Original'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_stock',
                'text',
                [
                    'name' => 'new_stock',
                    'label' => __('Stock'),
                    'id' => 'new_stock',
                    'title' => __('Stock'),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
        /** Inventario Fin */
        /** Envio Inicio */
        $fieldset->addField(
            'shipping',
            'text',
            [
                'name' => 'shipping',
                'label' => __('Envio Original'),
                'id' => 'shipping',
                'readonly' => 'readonly',
                'class' => 'traslate-text-hfx',
                'title' => __('Envio Original'),
                'style' => 'background-color: #dcdcdc;',
                ]
            );
            $fieldset->addField(
                'new_shipping',
                'text',
                [
                    'name' => 'new_shipping',
                    'label' => __('Envio'),
                    'id' => 'new_shipping',
                    'title' => __('Envio'),
                    'class' => 'required-entry',
                    'required' => true,
                    ]
                );
        /** Envio Fin */
        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
        /** Short Description Inicio */
        $fieldset->addField(
            'short_description',
            'textarea',
            [
                'name' => 'short_description',
                'label' => __('Short description Original'),
                'style' => 'height:12em;',
                'class' => 'traslate-text-hfx',
                'readonly' => 'readonly',
                'style' => 'background-color: #dcdcdc;',
            ]
        );
        $fieldset->addField(
            'new_short_description',
            'editor',
            [
                'name' => 'new_short_description',
                'label' => __('Short description'),
                'style' => 'height:12em; width:800px;',
                'required' => true,
                'config' => $wysiwygConfig
            ]
        );
        /** Short Description Fin */
        /** Long Description Inicio */
        $fieldset->addField(
            'long_description',
            'textarea',
            [
                'name' => 'long_description',
                'label' => __('Long description Original'),
                'style' => 'height:12em;',
                'class' => 'traslate-text-hfx',
                'data-type' => 'editor',
                'readonly' => 'readonly',
                'style' => 'background-color: #dcdcdc;',
            ]
        );
        $fieldset->addField(
            'urk',
            'hidden',
            [
                'name' => 'urk',
                'readonly' => 'readonly',
            ]
        );
        $fieldset->addField(
            'new_long_description',
            'editor',
            [
                'name' => 'new_long_description',
                'label' => __('Long description'),
                'style' => 'height:12em; width:800px;',
                'required' => true,
                'config' => $wysiwygConfig
            ]
        );
        /** Long Description Fin */
        /** Features Inicio */
        $fieldset->addField(
            'features',
            'textarea',
            [
                'name' => 'features',
                'label' => __('Features Original'),
                'class' => 'traslate-text-hfx',
                'data-type' => 'editor',
                'style' => 'height:12em;',
                'readonly' => 'readonly',
                'style' => 'background-color: #dcdcdc;',
            ]
        );
        $fieldset->addField(
            'new_features',
            'editor',
            [
                'name' => 'new_features',
                'label' => __('Features'),
                'style' => 'height:12em; width:800px;',
                'required' => true,
                'config' => $wysiwygConfig
            ]
        );
        /** Features Fin */
        $fieldset->addField(
            'image_url',
            'hidden',
            [
                'name' => 'image_url',
                'id' => 'image_url',
                ]
            );
            $fieldset->addField(
                'cat_product',
                'hidden',
                [
                    'name' => 'cat_product',
                    'id' => 'cat_product',
                    ]
                );
                $cat = $form->getElement('cat_product');
                $cat->setAfterElementHtml('<h2>Categorias</h2><hr><br>
                Categoria Original: Disc brake Pads
                ');
                $fieldset->addField(
                    'categories',
                    'hidden',
                    [
                        'name' => 'categories',
                        'id' => 'categories',
                        'class' => 'traslate-text-hfx',
                        ]
                    );
                $fieldset->addField(
                    'new_categories',
                    'select',
                    [
                        'name' => 'new_categories',
                        'label' => __('Categorias'),
                        'id' => 'new_categories',
                        'title' => __('Categorias'),
                        'values' => $this->_categorytree->toOptionArray(),
                        'class' => 'required-entry',
                        'required' => true,
                        ]
                    );


        $fieldset->addField(
            'rel_product',
            'hidden',
            [
                'name' => 'rel_product',
                'id' => 'rel_product',
                ]
            );
            
        $cat = $form->getElement('rel_product');
        $childs = '';
        if(!empty($dataExtra['childs'])){
            $childs = '<h2>Productos Relacionados</h2><hr>
            '.$dataExtra['childs'].'<br>'; 
        }
        $cat->setAfterElementHtml('
        <style>
        .header-hf{
            background-color:#ccc;
        }
        .header-hf th{ 
            background-color:#ccc;
            border: 1px solid #283747;
            padding:.5rem;
        }
        .table-child{
            border: 1px solid #ccc;
        }
        .table-child tr{
            border: 1px solid #ccc;
        }
        .table-child tr td{
            border: 1px solid #ccc;
            padding:.5rem;
        }
        
        .action-check{
            text-align:center;
        }
        .input-text-hf{
            width:98%;
        }
        </style>'.$childs.'
        <br><h2>Conjunto de atributos</h2><hr>
        '.$dataExtra['conjunto_atributos'].'
        <br><h2>Atributos</h2><hr>
        '.$dataExtra['atributos'].'
        '); 
        

        $fieldset->addField(
            'is_active',
            'hidden', 
            [
                'name' => 'is_active',
                'label' => __('Status'),
                'id' => 'is_active',
                'title' => __('Status'),
                'values' => $this->_options->getOptionArray(),
                'class' => 'status',
                'required' => true,
            ]
        );
         /** -------------------------------------- */

                /** Marca Modelo Inicio */
                $fieldset->addField(
                    'fieldsep2',
                    'hidden',
                    [
                        'name' => 'fieldsep2',
                        'id' => 'fieldsep2',
                        ]
                    );
                $cat = $form->getElement('fieldsep2');
                $cat->setAfterElementHtml('<br><h2>Vehiculos</h2><hr>
                '.$dataExtra['brands'].'
                ');
                
                $fieldset->addField(
                    'updatemanufacturer',
                    'hidden',
                    [
                        'name' => 'updatemanufacturer',
                        'id' => 'updatemanufacturer',
                        ]
                    );
                $fieldset->addField(
                    'fieldsep3',
                    'hidden',
                    [
                        'name' => 'fieldsep3',
                        'id' => 'fieldsep3',
                        ]
                    );
                   


                /** Marca Modelo Fin */

                /** Atributos Inicio */
               /* $fieldset->addField(
                    'fieldsep1',
                    'hidden',
                    [
                        'name' => 'fieldsep1',
                        'id' => 'fieldsep1',
                        ]
                    );
                $cat = $form->getElement('fieldsep1');
                $cat->setAfterElementHtml('<br><h2>Atributos</h2><hr>');*/

                /** Atributos Fin */
        $Lastfield = $form->getElement('fieldsep3');
        $siteUrl = $this->_storeManager->getStore()->getBaseUrl();
        //$find = "$('#wpgrid_new_marca_1').find('option[text=\"Mazada\"]').val()";
        $Lastfield->setAfterElementHtml(
                '<script type="text/javascript"> 
                require(["jquery", "jquery/ui"], function($){ 
                    $(document).ready(function () {
                        $("#wpgrid_new_attributeset").change(function(){
                            $("#msgLoaderHf").html("Cargando");
                                $(".loading-mask-HF").show();
                                $.ajax({
                                    method: "POST",
                                    url: "'.$siteUrl.'grid/index/rulesdata",
                                    data: { data: {rule:2,tipodato:7,value:$(this).val(),type:"formIndex"}},
                                    dataType: "json"
                                    })
                                .done(function( msg ) {
                                    var optionsAtt = "";
                                    $(msg).each(function( indexAtt,valueAtt ) {
                                        optionsAtt += "<optgroup label=\""+ valueAtt.name +"\">";
                                        $(valueAtt.options).each(function( optionId,optionValue ) {
                                            optionsAtt += "<option   value=\""+ optionValue.id +"\">"+optionValue.name +"</option>";
                                        })
                                        optionsAtt += "</optgroup>";
                                    });
                                    $(".attributos_producto_hf").each(function(index,value){  
                                        $(this).html(optionsAtt);
                                    });
                                    $(".loading-mask-HF").hide();
                                 });
                            });
                        var vManufacturer = "'.$model->getNewManufacturer().'";
                        console.log("vManufacturer: "+vManufacturer);
                        if(vManufacturer!=""){
                            $("#wpgrid_new_manufacturer").change(function(){
                               require([
                                    "Magento_Ui/js/modal/confirm"
                                ], function(confirmation) { // Variable that represents the `confirm` widget

                                    confirmation({
                                        title: "Alerta",
                                        content: "Usted esta intentando cambiar un atributo predefinido, esto afectará a todos los productos relacionados a dicha categoria. ¿esta seguro que desea hacer esto?",
                                        actions: {
                                            confirm: function(){
                                                $("#wpgrid_updatemanufacturer").val("actualizar");
                                            },
                                            cancel: function(){
                                            },
                                            always: function(){
                                            }
                                        }
                                    });
                                });
                            });
                        }
                        var idCopy = "";
                        var idImput = "";
                        if($("#wpgrid_is_active").val()!="2"){
                        $( ".copy-data" ).each(function( index,value ) {
                            idCopy= $(this).attr("id").split("wpgrid_");
                            if($("#wpgrid_new_"+idCopy[1]+"").val()==""||$("#wpgrid_new_"+idCopy[1]+"").val()=="0"){
                                $("#wpgrid_new_"+idCopy[1]+"").val($(this).val());
                            }
                          });
                        }
                    var fields = "";
                     $( ".traslate-text-hfx" ).each(function( index,value ) {
                      //  fields += $(this).attr("id")+"@&nbsp;@"+$(this).val()+"@&nbsp;@"+$(this).data("type")+"#&nbsp;#"; 
                        fields += $(this).attr("id")+"@&nbsp;@"+$(this).val()+"#&nbsp;#"; 
                      });
                      $("#msgLoaderHf").html("Traduciendo, por favor espere");
                     $(".loading-mask-HF").show();
                        $.ajax({
                                  method: "POST",
                                  url: "'.$siteUrl.'grid",
                                  data: { traducir: fields},
                                  dataType: "json"
                                })
                              .done(function( msg ) {
                                $("#togglewpgrid_new_short_description").trigger("click");
                                $("#togglewpgrid_new_long_description").trigger("click");
                                $("#togglewpgrid_new_features").trigger("click");
                                $(msg).each(function( index,value ) {
                                    //console.log("data: "+value[1]);
                                    if(value[1]!=""){
                                        var sp1 = value[0];
                                        var sp = sp1.split("_");
                                        console.log(sp);
                                        if(sp[0]=="marca"){
                                           // console.log("Campo: "+value[0]);
                                            //console.log("Data: "+value[1]);
                                            $("#wpgrid_new_"+value[0]+" > option").each(function() {
                                            //console.log($(this).text());
                                            //console.log($(this).val());
                                            if(value[1]==$(this).text()){
                                                $("#wpgrid_new_"+value[0]+"").val($(this).val());
                                            }
                                            });
                                        }else if(sp[0]=="modelo"){
                                            var optionsModelo  = "";
                                            $(value[1]).each(function(index,valueEach) {
                                                var selectUnique = "";
                                                if(valueEach.id==value[2]){
                                                    selectUnique = "selected";
                                                }
                                                //console.log(value.id);
                                                optionsModelo += "<option "+selectUnique+"  value=\""+ valueEach.name +"\">"+valueEach.name +"</option>";
                                            });
                                            $("#wpgrid_new_"+value[0]+"").append(optionsModelo);
                                        }else if(sp[0]=="submodelo"){
                                            var optionsSubModelo  = "";
                                            $(value[1]).each(function(index,valueEach) {
                                                var selectUnique = "";
                                                if(valueEach.id==value[2]){
                                                    selectUnique = "selected";
                                                }
                                                if(valueEach.name==value[2]){
                                                    selectUnique = "selected";
                                                }
                                                //console.log(value.id);
                                                optionsSubModelo += "<option "+selectUnique+"  value=\""+ valueEach.name +"\">"+valueEach.name +"</option>";
                                            });
                                            $("#wpgrid_new_"+value[0]+"").append(optionsSubModelo);
                                        }else if(sp[0]=="attr"){
                                            var optionsAtt = "";
                                            var selectedAtt = "";
                                            $(value[1]).each(function( indexAtt,valueAtt ) {
                                                optionsAtt += "<optgroup label=\""+ valueAtt.name +"\">";
                                                $(valueAtt.options).each(function( optionId,optionValue ) {
                                                    selectedAtt = "";
                                                    if(optionValue.id==value[2]){
                                                        console.log(optionValue);
                                                        selectedAtt = "selected";
                                                    }   
                                                    optionsAtt += "<option "+selectedAtt+"  value=\""+ optionValue.id +"\">"+optionValue.name +"</option>";
                                                })
                                                optionsAtt += "</optgroup>";
                                            });
                                            $("#wpgrid_new_"+value[0]+"").html(optionsAtt);
                                        }else{
                                            $("#wpgrid_new_"+value[0]+"").val(value[1]);
                                        }
                                    }
                                  });
                                  $("#msgLoaderHf").html("Traducción completa"); 
                                  $(".loading-mask-HF").hide();
                                //do something with you return data 
                              });
                            /** Save in magento function inicio */
                            
                            $("#saveinmagento").click(function(){

                                    if ($("#edit_form").valid()) {
                                        $.fn.serializeControls = function() {
                                            var data = {};
                                          
                                            function buildInputObject(arr, val) {
                                              if (arr.length < 1)
                                                return val;  
                                              var objkey = arr[0];
                                              if (objkey.slice(-1) == "]") {
                                                objkey = objkey.slice(0,-1);
                                              }  
                                              var result = {};
                                              if (arr.length == 1){
                                                result[objkey] = val;
                                              } else {
                                                arr.shift();
                                                var nestedVal = buildInputObject(arr,val);
                                                result[objkey] = nestedVal;
                                              }
                                              return result;
                                            }
                                          
                                            $.each(this.serializeArray(), function() {
                                              var val = this.value;
                                              var c = this.name.split("[");
                                              var a = buildInputObject(c, val);
                                              $.extend(true, data, a);
                                            });
                                            
                                            return data;
                                          }
                                          var arrays = JSON.stringify($("#edit_form").serializeControls(), null, 2);
                                          console.log(arrays);
                                         // return false;
                                       // var params = $("#edit_form").serialize();
                                        $("#msgLoaderHf").html("Creando producto, por favor espere");
                                     $(".loading-mask-HF").show();
                                        $.ajax({
                                            method: "POST",
                                            url: "'.$siteUrl.'grid/index/product/",
                                            data: { groups: arrays},
                                            dataType: "json"
                                          })
                                        .done(function( msg ) {

                                            /*$(msg).each(function( index,value ) {
                                                console.log(value);
                                          });*/
                                          $("#msgLoaderHf").html("Producto creado correctamente");
                                          $(".loading-mask-HF").hide();
                                          window.location.href = "'.$siteUrl.'admin_autoparts/grid/grid/index";
                                        });
                                        return false;
                                    }else{
                                        return false;
                                    }
                                });
                                $("#massdefault").click(function(){
                                    if($(this).is(":checked") == true){
                                        $(".default").prop("checked",true);
                                    }else{
                                        $(".default").prop("checked",false);
                                    }
                                });
                                $("#massrel").click(function(){
                                    if($(this).is(":checked") == true){
                                        $(".rel").prop("checked",true);
                                    }else{
                                        $(".rel     ").prop("checked",false);
                                    }
                                });
                                /** Desplegables Model y Submodelo Inicio */
                                $(".select-brand").change(function(){
                                        $("#msgLoaderHf").html("Cargando");
                                        $(".loading-mask-HF").show();
                                        var obj = $(this).data("target");
                                        $.ajax({
                                            method: "POST",
                                            url: "'.$siteUrl.'grid/index/rulesdata",
                                            data: { data: {rule:1000,tipodato:2000,target:obj,value:$(this).val(),dropdown:$(this).data("dropdown"),dropdownnext:$(this).data("dropdownnext")}},
                                            dataType: "json"
                                            })
                                        .done(function( msg ) {
                                            var selectUnique = "";
                                            console.log(msg[0]);
                                            if(msg.length==1&&msg[0].name=="NA"){
                                                 selectUnique = "selected";
                                            }
                                            //console.log(msg);
                                           var options = "<option value=\"\">Seleccione</option>";
                                            $(msg).each(function( index,value ) {
                                                options += "<option "+selectUnique+" value=\""+ value.id +"\">"+value.name +"</option>";
                                            });
                                            //   console.log(options);
                                            console.log(obj);
                                            $("#"+obj+"").html(options);
                                            $(".loading-mask-HF").hide();
                                        });
                                    });
                                /** Desplegables Model y Submodelo Fin */
                            /** Save in magento function Fin */
                });
                });
                </script>
                <div style="display:none" class="loading-mask loading-mask-HF" data-role="loader">
                <div class="loader">
                    <p id="msgLoaderHf" ></p>
                    </div>
                </div>'
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
