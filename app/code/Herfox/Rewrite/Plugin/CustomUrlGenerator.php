<?php

namespace Herfox\Rewrite\Plugin;

class CustomUrlGenerator {

   // public function beforeGetUrlKey(\Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $subject, $product)
   
   public function aroundGetUrlKey(\Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator $subject, callable $process, \Magento\Catalog\Model\Product $product)
   {
        /* $urlKey = $product->getUrlKey();
         if ($urlKey === '' || $urlKey === null) {
              $product->setUrlKey($product->getName() . "/" . $product->getId());
         }
            return ['getUrlKey' => $product];*/
            $urlKey = $product ? $product->getUrlKey() : '';
            if ($urlKey === '' || $urlKey === null) {
                $product->setUrlKey($product->getName() . "/" . $product->getId());
            }
            return $process($product);
    }
}