<?php
namespace Meigee\Porter\Helper;

class PorterBgSlider extends Section
{
    protected function getThemeCnfBlock()
    {
        return 'porter_bg_slider';
    }
} 