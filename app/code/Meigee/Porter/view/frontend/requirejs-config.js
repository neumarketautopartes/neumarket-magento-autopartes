var config = {
    map: {
        '*': {
            MeigeeBootstrap: 'Meigee_Porter/js/bootstrap.min',
            MeigeeCarousel: 'Meigee_Porter/js/owl.carousel',
            jqueryBackstretch: 'Meigee_Porter/js/jquery.backstretch.min',
			googleMap: 'https://maps.googleapis.com/maps/api/js?sensor=false',
//            lightBox: 'Meigee_Porter/js/ekko-lightbox.min'
           lightBox: 'Meigee_Porter/js/ekko-lightbox.customized',
		   mobile_menu: 'Meigee_Porter/js/mobile_menu',
		   sticky_menu: 'Meigee_Porter/js/sticky_menu'
        }
    }

};




