<?php
namespace Meigee\Porter\Block\Adminhtml\System\Config\InputRadio;

class Vertical extends \Meigee\Porter\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-vertical';
    }
} 