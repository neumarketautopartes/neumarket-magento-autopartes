<?php
namespace Meigee\Porter\Block\Adminhtml\System\Config\InputRadio;

class Horizontal extends \Meigee\Porter\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-horizontal';
    }
} 