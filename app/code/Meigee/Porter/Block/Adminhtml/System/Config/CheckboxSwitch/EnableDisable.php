<?php
namespace Meigee\Porter\Block\Adminhtml\System\Config\CheckboxSwitch;

class EnableDisable extends \Meigee\Porter\Block\Adminhtml\System\Config\CheckboxSwitch
{
    function getOnLabel()
    {
        return __('Enable');
    }
    function getOffLabel()
    {
        return __('Disable');
    }
}