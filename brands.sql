-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-11-2018 a las 01:00:00
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(1, 'Alfa Romeo'),
(2, 'Audi'),
(3, 'BMW'),
(4, 'Chevy'),
(5, 'Chrysler'),
(6, 'Citroen'),
(7, 'Daewoo'),
(8, 'Dodge'),
(9, 'Ferrari'),
(10, 'Fiat'),
(11, 'Ford'),
(12, 'Honda'),
(13, 'Hyundai'),
(14, 'Jaguar'),
(15, 'Jeep'),
(16, 'Kia'),
(17, 'Land Rover'),
(18, 'Mahindra'),
(19, 'Maserati'),
(20, 'Mazda'),
(21, 'Mercedes'),
(22, 'MG'),
(23, 'Mini'),
(24, 'Mitsubishi'),
(25, 'Nissan'),
(26, 'Peugeot'),
(27, 'Porsche'),
(28, 'Renault'),
(29, 'Seat'),
(30, 'Skoda'),
(31, 'Subaru'),
(32, 'Suzuki'),
(33, 'Toyota'),
(34, 'Volkswagen'),
(35, 'Volvo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
